export default function(state, action){
  switch(action.type){
      case 'SET_SESSION':
          return{
                ...state,
                appComission: action.payload.appComission,
                bid: action.payload.bid,
                call: {
                    authToken: "",
                    willTopic: "",
                },
                countryCode: action.payload.countryCode,
                email: action.payload.email,
                fcmTopic: action.payload.fcmTopic,
                firstName: action.payload.firstName,
                hasLogin: action.payload.hasLogin,
                id: action.payload.id,
                lastName: action.payload.lastName,
                mobile: action.payload.mobile,
                profilePic: action.payload.profilePic,
                referralCode: action.payload.referralCode,
                requester_id: action.payload.requester_id,
                status: action.payload.status,
                token: action.payload.token,
                expoToken:''
          }
      case 'SET_PROFILE':
        return{
            ...state,
            countryCode: action.payload.countryCode,
            firstName: action.payload.firstName,
            lastName: action.payload.lastName,
            mobile: action.payload.phone,
            profilePic: action.payload.profilePic
      }
      case 'SET_PICTURE':
          return{
              ...state,
              profilePic: action.payload
          }
      case 'SET_ADDRESS':
          return{
              ...state,
              address: action.payload
          }
      case 'SET_LOCATION':
          return{
              ...state,
              latitude: action.payload.latitude,
              longitude: action.payload.longitude
          }
      case 'SET_STATUS':
          return{
              ...state,
              status: action.payload
          }
      case 'SET_EXPO_TOKEN':
          return{
              ...state,
              expoToken: action.payload
          }
      case 'CLEAR':
          return{
                ...state,
                appComission: null,
                bid: null,
                call: {
                    authToken: "",
                    willTopic: "",
                },
                countryCode: null,
                email: null,
                fcmTopic: null,
                firstName: null,
                hasLogin: false,
                id: null,
                lastName: null,
                mobile: null,
                profilePic: null,
                referralCode: null,
                requester_id: null,
                status: null,
                token: null,
                expoToken: null
          }
      default:
          return state;
  }
}