export default function(state, action){
  switch(action.type){
      case 'SET_DEVICE':
          return{
              ...state,
              appBuild: action.payload.appBuild,
              appId: action.payload.appId,
              appName: action.payload.appName,
              appVersion: action.payload.appVersion,
              isVirtual: false,
              manufacturer: action.payload.manufacturer,
              model: action.payload.model,
              operatingSystem: action.payload.operatingSystem,
              osVersion: action.payload.osVersion,
              platform: action.payload.platform,
              uuid: action.payload.uuid
          }
      default:
          return state;
  }
}