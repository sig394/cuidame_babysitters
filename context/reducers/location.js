export default function(state, action){
  switch(action.type){
      case 'SET_LOCATION':
          return{
              ...state,
              latitude: action.payload.latitude,
              longitude: action.payload.longitude
          }
      default:
          return state;
  }
}