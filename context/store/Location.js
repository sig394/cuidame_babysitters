import React, {useReducer, createContext, useEffect } from 'react';
import * as Location from 'expo-location';
import locationReducer from '../reducers/location';

export const LocationContext = createContext();

const getLocation = async() =>{
  let { status } = await Location.requestForegroundPermissionsAsync();
  if (status !== 'granted') {
      console.log('Permission to access location was denied');
      return {};
  }

  let location = await Location.getCurrentPositionAsync({enableHighAccuracy:true});
  return location.coords;
}

const LocationContextProvider = (props) => {

  const [location, dispatchLocation ] = useReducer(locationReducer, {});

  useEffect(() => {
      async function fetchLocation() {
        const location = await getLocation();
        dispatchLocation({type: 'SET_LOCATION', payload:location});
        
      }
      fetchLocation();
   }, []);

  return(
      <LocationContext.Provider value={{location,dispatchLocation}}>
          {props.children}
      </LocationContext.Provider>
  )
}

export default LocationContextProvider;