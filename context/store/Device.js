import React, {useReducer, createContext } from 'react';
import deviceReducer from '../reducers/device';

export const DeviceContext = createContext();


const DeviceContextProvider = (props) => {


    const [device, dispatchDevice ] = useReducer(deviceReducer, {});

    return(
        <DeviceContext.Provider value={{device,dispatchDevice}}>
            {props.children}
        </DeviceContext.Provider>
    )
}

export default DeviceContextProvider;