import React, {useReducer, createContext, useEffect } from 'react';
import sessionReducer from '../reducers/session';
import AsyncStorage from '@react-native-async-storage/async-storage';

export const SessionContext = createContext();


const getSession = async () => {
    try {
      const session = await AsyncStorage.getItem('session')
      return session ? JSON.parse(session) : {};
    } catch (e) {
      console.log('Failed to fetch the session data from storage');
    }
}

const SessionContextProvider = (props) => {

    const [session, dispatchSession ] = useReducer(sessionReducer, {});

    useEffect(() => {
        async function fetchSession() {
          const session = await getSession();
          dispatchSession({type: 'SET_SESSION', payload:session});
          
        }
        fetchSession();
     }, []);

    return(
        <SessionContext.Provider value={{session,dispatchSession}}>
            {props.children}
        </SessionContext.Provider>
    )
}

export default SessionContextProvider;

