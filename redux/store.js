import {createStore, combineReducers, applyMiddleware} from 'redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { persistStore, persistReducer } from 'redux-persist';
import thunk from 'redux-thunk';
import globalReducer from './reducers';

const persistConfig = {
    key: 'session',
    storage: AsyncStorage
}

const rootReducer = combineReducers({
    globalReducer: persistReducer(persistConfig, globalReducer)
})

export const Store = createStore(rootReducer, applyMiddleware(thunk));
export const persistor = persistStore(Store);
