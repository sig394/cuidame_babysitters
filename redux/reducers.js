import { SET_DEVICE, SET_LOCATION,SET_PROFILE,SET_SESSION, LOGOUT } from "./actions";

const initialState = {
    session: null,
    profile: null,
    location: null
}

function globalReducer(state = initialState,action){
    switch(action.type){
        case SET_SESSION:
            return {...state, session:action.payload};
        case SET_LOCATION:
            return {...state, location:action.payload};
        case SET_PROFILE:
            return {...state, profile: action.payload};
        case SET_DEVICE:
            return {...state, device: action.payload};
        case LOGOUT:
            return initialState;
        default:
            return state;
    }
}

export default globalReducer;