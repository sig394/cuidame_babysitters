export const SET_SESSION = 'SET_SESSION'
export const SET_STATE = 'SET_STATE'
export const SET_PROFILE = 'SET_PROFILE'
export const SET_LOCATION = 'SET_LOCATION'
export const SET_DEVICE = 'SET_DEVICE'
export const LOGOUT = 'LOGOUT'

export const setLocation = location => dispatch =>{
    dispatch({
        type: SET_LOCATION,
        payload: location
    })
};

export const setSession = session => dispatch =>{
    dispatch({
        type: SET_SESSION,
        payload: session
    });
};

export const setStatus = status => dispatch =>{
    dispatch({
        type: SET_STATE,
        payload: status
    });
}

export const setProfile = profile => dispatch =>{
    dispatch({
        type: SET_PROFILE,
        payload: profile
    })
};

export const setDevice = device => dispatch =>{
    dispatch({
        type: SET_DEVICE,
        payload: device
    })
};

export const setLogout = () => dispatch =>{
    dispatch({
        type: LOGOUT,
        payload: null
    })
}
