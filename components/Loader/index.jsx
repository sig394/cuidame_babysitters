import React from 'react';
import { Text, StyleSheet } from 'react-native';
import AnimatedLoader from "react-native-animated-loader";

const Loader = (props) => {

      return (
        <AnimatedLoader
            visible={props.visible}
            overlayColor="rgba(255,255,255,0.75)"
            source={props.map ? require("./location-searching.json") : require("./loader-circles.json")}
            animationStyle={props.map ? styles.map : styles.default}
            speed={1}
        >
            <Text>{props.text ? props.text : "Cargando..."}</Text>
        </AnimatedLoader>
    );
}

const styles = StyleSheet.create({
    default: {
      width: 100,
      height: 100
    },
    map:{
      width: 200,
      height: 200
    }
});

export default Loader;