import React, { useState } from 'react';
import { View,TextInput } from 'react-native';

import CommonStyles from '../utils/CommonStyles';

const InputText = ({value, placeholder,onBlur,onChange,isPassword}) => {

  const [isSecure, setIsSecure] = useState(isPassword);

  return (
    <View style={CommonStyles.inputContainer}>
      <TextInput
        style={CommonStyles.inputStyle}
        placeholder={placeholder}
        onBlur={onBlur}
        onChangeText={val => onChange(val)}
        value={value}
        secureTextEntry={isSecure}
      />
      {/*
        isPassword &&
        <Icon name={isSecure ? 'eye-off' : 'eye'}  onPress={()=>setIsSecure(!isSecure)} />
      */}
    </View>
  );
}
 
export default InputText;