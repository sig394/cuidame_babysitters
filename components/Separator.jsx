import React from 'react';
import { View } from 'react-native';
import { DARK_GRAY } from '../utils/Constants';

const Sepatator = () => {
    return (
        <View style={{borderTopWidth:1, width:'100%', borderTopColor:DARK_GRAY}} />
    );
}
 
export default Sepatator;