import React from 'react';
import {View} from 'react-native';
import { GRAY, LIGHT_BLACK } from '../utils/Constants';

const Separator = () => {
    return (
        <View
            style={{
                borderBottomColor: GRAY,
                borderBottomWidth: 1,
                marginHorizontal: 5,
                marginVertical:10
            }}
        />
    );
}
 
export default Separator;