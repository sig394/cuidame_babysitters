import React, { useEffect, useState, useRef } from 'react';
import { StyleSheet,TextInput,Text } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { DARK_GRAY } from '../../utils/Constants';


const BigInput = ({prefix, onValueChange, inputRef, inValue, min, max}) => {

    const [value, setValue] = useState('');
    const InputRef = useRef(null);

    const setInputValue = (val) =>{
        let formatValue = val.replace(".","").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        setValue(formatValue);
        onValueChange(val.replace(".",""));
    }

    const checkRate = () =>{
        let cleanVal = value.replace(".","")
        if(parseInt(cleanVal) >= min && parseInt(cleanVal) <= max){   
        }else{
            setInputValue("6000")
        }
    }

    useEffect(()=>{
        InputRef.current.focus();
    },[])

    useEffect(()=>{
        if(inValue){
            let formatValue = inValue.toString().replace(".","").replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            setValue(formatValue)
        }
    },[inValue])

    return (
        <TouchableOpacity activeOpacity={1} style={styles.inputContainer} onPress={()=> InputRef.current.focus()}>
            <Text style={styles.prefix}>{prefix}</Text>
            <TextInput
                ref={InputRef}
                style={styles.inputText}
                placeholder="Monto"
                keyboardType="number-pad"
                underlineColorAndroid="transparent"
                value={value}
                textAlign={'center'}
                maxLength={9}
                onChangeText={val => setInputValue(val)}
                onBlur={()=> {
                    if(min && max){
                        checkRate()
                    }
                }}
            />
        </TouchableOpacity>
    );
}
 
export default BigInput;

const styles = StyleSheet.create({
    inputText:{
        fontSize:32,
        color: DARK_GRAY,
        
    },
    inputContainer: {
        width:200,  
        borderWidth: 1,
        borderColor:DARK_GRAY,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: 'white',
        marginHorizontal: 10,
        marginVertical: 10,
        borderRadius: 10,
        height:80,
        
    },
    prefix: {
      paddingHorizontal: 10,
      fontWeight: 'bold',
      color: DARK_GRAY,
      fontSize:34
    }
})
 