import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    card:{
        padding: 10,
        borderRadius:6,
        elevation: 3,
        backgroundColor:'#fff',
        shadowOffset:{
            width:1,
            height:1
        },
        shadowColor:'#333',
        shadowOpacity:0.3,
        shadowRadius:2,
        marginVertical:10,
        maxHeight: 160
    },
    cardContent:{
        marginHorizontal:18,
        marginVertical:10,
    },
    row:{
        flex:1,
        flexDirection:'row'
    },
    title:{
        fontSize:16,
        fontWeight:'bold'
    }
});