import React from 'react';
import Styles from './Styles';
import { View, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { LIGHT_BLACK } from '../../utils/Constants';
import { Avatar } from 'react-native-elements'
import { numberFormat, toLongDate } from '../../utils/Utils';
import CommonStyles from '../../utils/CommonStyles';

const BookingCard = (props) => {

    const goToProgress = (r) =>{
        console.log("RESERVATION", r.bookingId)
        props.navigation.navigate('BookingProgress',{
            reservation: r
        })
    } 

    const getStatusText = (status) =>{
        const statusLower = status.toLowerCase();
        if(statusLower === 'pending'){
            return "Pendiente";
        }else if(statusLower === 'accepted'){
            return "Aceptado";
        }else if(statusLower === 'on the way'){
            return "En camino";
        }else if(statusLower === 'arrived'){
            return "Llegó";
        }else if(statusLower === 'job started'){
            return "Iniciado";
        }else{
            return "Finalizado";
        }

    }

    return (
        <TouchableOpacity activeOpacity={1} style={Styles.card} onPress={()=> goToProgress(props.reservation)}>
            <>
                <View style={Styles.row}>
                    <View style={{width:'70%', alignItems:'flex-start'}}>
                        <Text style={Styles.title}>Cliente</Text>
                    </View>
                    <View style={{width:'30%', alignItems:'flex-end'}}>
                        <Text style={Styles.title}>${numberFormat(props.reservation.accounting.amount)}</Text>
                    </View>
                </View>
                <View style={Styles.row}>
                    <Text>{toLongDate(props.reservation.bookingRequestedAt, "es")}</Text>
                </View>
                <View style={Styles.row}>
                    <Icon name={'md-location'} size={16} color={LIGHT_BLACK} />
                    <Text>{props.reservation.addLine1} {props.reservation.addLine2}</Text>
                </View>
                <View style={Styles.row}>
                    <View style={{width:'15%'}}>
                        <Avatar rounded 
                            size="small" 
                            source={{uri: props.reservation.profilePic}} 
                        />
                    </View>
                    <View style={{width:'55%', alignItems:'flex-start', marginTop:5}}>
                        <Text>{props.reservation.firstName + ' ' + props.reservation.lastName}</Text>
                    </View>
                    <View style={{width:'30%', alignItems:'flex-end', marginTop:5}}>
                        <Text style={CommonStyles.primaryText}>
                            {getStatusText(props.reservation.statusMsg)}
                        </Text>
                    </View>
                </View>
            </>
        </TouchableOpacity>
    );
}
 
export default BookingCard;