import axiosInstance from "../api/AxiosInstance"

function formEncoded(data){
    let formData = new FormData();
    Object.keys(data).forEach((key, index)=>{
      formData.append(key,data[key])
    })

    return formData;
}

export const getNotifications = async () => {
    const {data} = await axiosInstance.get('provider/notification/0/10')
    return data.hasOwnProperty('data') ? data.data : null;
}

export const updateStatus = async(payload) =>{
    return await axiosInstance.patch('provider/status',payload)
}

export const getWalletData = async() =>{
    return await axiosInstance.get('provider/paymentsettings')
}

export const getWalletTransactions = async() =>{
    return await axiosInstance.get('provider/wallet/transction/0')
}

export const patchProfile = async(payload) =>{
    return await axiosInstance.patch('provider/profile/me', payload);
}

export const patchPassword = async(payload) =>{
    return await axiosInstance.patch('provider/password/me', payload);
}

export const getReviews = async() =>{
    return await axiosInstance.get('provider/reviewAndRating/0')
}

export const getAddress = async() =>{
    return await axiosInstance.get('provider/address')
}

export const saveAddress = async(payload) =>{
    return await axiosInstance.post('provider/address', payload)
}

export const deleteAddress = async(id) =>{
    return await axiosInstance.delete('provider/address/'+id)
}

export const saveExpoToken = async(payload) =>{
    return await axiosInstance.post('provider/notifications', payload);
}

export const saveRate = async(payload) =>{
    return await axiosInstance.patch('provider/service', payload);
}

export const getProviderProfile  = async() =>{
    return await axiosInstance.get('provider/profile/me')
}

export const getProviderCategory = async() =>{
    return await axiosInstance.get('provider/category')
}

export const getUpcomingBookings = async() =>{
    return await axiosInstance.get('provider/booking')
}

export const getBookingById = async(id) =>{
    return await axiosInstance.get(`provider/booking/${id}`)
}

export const changeBookingStatus = async(payload) =>{
    return await axiosInstance.patch('provider/bookingResponse', payload)
}

export const providerBookingStatus = async(payload)=>{
    return await axiosInstance.patch('provider/bookingStatus', payload)
}

