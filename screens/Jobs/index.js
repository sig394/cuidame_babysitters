import React, { useEffect, useState } from 'react';
import { View, Text, Image,useWindowDimensions, ScrollView } from 'react-native';
import { useQuery } from 'react-query';
import Loader from '../../components/Loader';
import { getUpcomingBookings } from '../../services';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { PRIMARY_COLOR } from '../../utils/Constants';
import Styles from './Styles';
import BookingCard from '../../components/BookingCard';

const Jobs = (props) => {

  const layout = useWindowDimensions();
  const [pending, setPending] = useState(null)
  const [upcoming, setUpcoming] = useState(null);

  const {status, data:bookingData, refetch} = useQuery('all-bookings', ()=> getUpcomingBookings(),{
    
  })

  const noRecords = () =>{
    return(
        <View style={Styles.container}>
            <View style={Styles.row}>
                <Image source={require('../../assets/my_jobs.png')} style={{width:200}} resizeMode={'center'} />
            </View>
        </View>
    )
  }

  const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'current', title: 'Abierto' },
        { key: 'asigned', title: 'Asignado' },
  ]);

  const pendingReservations = () => {
    return(noRecords())
  }

  const currentReservations = () => {
    return(
      <>
            {
                bookingData.data.data && bookingData.data.data.hasOwnProperty("accepted") !== null
            ?
                (
                    <ScrollView
                            
                        >
                        <View style={{backgroundColor: '#fff',alignItems:'center',justifyContent:'center', padding:15 }}>
                            
                                {bookingData.data.data.accepted.map((b, idx)=>{
                                    return(
                                        <BookingCard key={idx} reservation={b} {...props} />
                                    )
                                })}
                            
                        </View>
                    </ScrollView>
                )
            :
                (
                    noRecords()
                )
            }
        </>
    )
  }

  useEffect(()=>{
    const unsubscribe = props.navigation.addListener('focus', () => {
      if(refetch){
        refetch()
      }
    });
  
    return unsubscribe;
  },[])

  /*useEffect(()=>{
    if(status === 'success'){
      if(bookingData.data.data && bookingData.data.data.hasOwnProperty("accepted")){
        setUpcoming(bookingData.data.data.accepted)
      }

      if(bookingData.data.data && bookingData.data.data.hasOwnProperty("activeBid")){
        setPending(bookingData.data.data.activeBid)
      }
    }
  },[status])*/


  if(status === 'success'){
    console.log(bookingData.data.data)
    return (
      <TabView
        navigationState={{ index, routes }}
        renderScene={SceneMap({
            current: pendingReservations,
            asigned: currentReservations
        })}
        onIndexChange={setIndex}
        initialLayout={{ width: layout.width }}
        renderTabBar={props => 
            <TabBar {...props} 
                style={{backgroundColor: '#fff'}}
                renderLabel={({route, color}) => (
                <Text style={{ color: PRIMARY_COLOR}}>
                    {route.title}
                </Text>
            )}
            indicatorStyle={{backgroundColor:PRIMARY_COLOR}}
        />}
      />
    );
  }else{
    return(
      <Loader 
        visible={true}
        textContent={"Procesando..."}
        textStyle={{color:"#FFF"}}
      />
    )
  }
}
 
export default Jobs;