import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    container: {
        flex:1,
        padding:20,
        backgroundColor:'#FFF',
        alignItems:'center',
        justifyContent:'center'
    },
    row:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        
    },
    halfColumn:{
        width:'50%',
        padding:5,
        alignSelf:'center'
    },

    bottom: {
        position: 'absolute',
        bottom:5
    }
})