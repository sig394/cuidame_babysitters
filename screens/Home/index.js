import React, {useState, useContext, useEffect, useRef} from 'react';
import { View, Dimensions, Platform, Text, StyleSheet ,TouchableOpacity, Alert, ScrollView} from 'react-native';
import * as Notifications from 'expo-notifications';
import Constants from 'expo-constants';
import Styles from './Styles';
import CommonStyles from '../../utils/CommonStyles';
import { useMutation } from 'react-query';
import { DARK_GRAY, GREEN, LIGHT_BLACK, PRIMARY_COLOR, RED_ERROR } from '../../utils/Constants';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Fontisto from 'react-native-vector-icons/Fontisto';
import { updateStatus, saveExpoToken, getUpcomingBookings } from '../../services';
import { useQuery } from 'react-query';
import { SessionContext } from '../../context/store/Session';
import { ListItem,Rating,Avatar } from 'react-native-elements'
import { numberFormat, toHourHumanReadable, toHumanReadable, toLongDate } from '../../utils/Utils';
import Loader from '../../components/Loader';
import MapView, {  Marker } from 'react-native-maps'; 
import { LocationContext } from '../../context/store/Location';

const { width, height } = Dimensions.get('window');
const parents = Math.floor(Math.random() * 20) + 1

Notifications.setNotificationHandler({
  handleNotification: async () => ({
    shouldShowAlert: true,
    shouldPlaySound: true,
    shouldSetBadge: true,
  }),
});

const Home = (props) => {

  const mapRef = useRef()
  const {session, dispatchSession} = useContext(SessionContext)
  const {location } = useContext(LocationContext)
  const MINUTE_MS = 10000;
  const ASPECT_RATIO = width / height;
  const LATITUDE_DELTA = 0.200;
  const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;
  const notificationListener = useRef();
  const responseListener = useRef();

  const {status, data:bookingData, refetch} = useQuery('bookings', ()=> getUpcomingBookings(),{
  })

  
  const [loading, setLoading] = useState(false);
  const [expoPushToken, setExpoPushToken] = useState('');
  const [notification, setNotification] = useState(false);
  const [hasBid, setHasBid] = useState(false)

  const expoTokenMutation = useMutation((payload)=>{
    return saveExpoToken(payload)
  })

  const statusMutation = useMutation((payload) =>{
    return updateStatus(payload)
  })

  const changeStatus = () =>{
    const newState = session.status === 4 ? 3 : 4
    const payload = {
      status: newState,
      latitude: "0",
      longitude: "0"
    }
    
    statusMutation.mutate(payload, {
      onSuccess: (data, variables, context) => {
        console.log(data)
        dispatchSession({type:'SET_STATUS', payload: newState});
      },
      onError: (error, variables, context) => {
        console.log(error);
      },
    });
  }

  async function registerForPushNotificationsAsync() {
    let token;

    if (Constants.isDevice) {
      
      console.log("Requesting token");
      const { status: existingStatus } = await Notifications.getPermissionsAsync();
      let finalStatus = existingStatus;
      if (existingStatus !== 'granted') {
        const { status } = await Notifications.requestPermissionsAsync();
        finalStatus = status;
      }
      if (finalStatus !== 'granted') {
        alert('Failed to get push token for push notification!');
        return;
      }
      token = (await Notifications.getExpoPushTokenAsync()).data;
      console.log(token);
      
      const expoPayload = {
        expoToken: token
      }
      expoTokenMutation.mutate(expoPayload, {
        onSuccess: (data, variables, context) => {
          
        },
        onError: (error, variables, context) => {
          console.log("SaveExpoToken:",error);
        },
      });
      
    } else {
      alert('Must use physical device for Push Notifications');
    }
  
    if (Platform.OS === 'android') {
      Notifications.setNotificationChannelAsync('default', {
        name: 'default',
        importance: Notifications.AndroidImportance.MAX,
        vibrationPattern: [0, 250, 250, 250],
        lightColor: '#FF231F7C',
      });
    }
    
  
    return token;
  }

  useEffect(()=>{
    registerForPushNotificationsAsync().then(token => {
      setExpoPushToken(token)
    });

    notificationListener.current = Notifications.addNotificationReceivedListener(notif => {
      setNotification(notif);
    });

    responseListener.current = Notifications.addNotificationResponseReceivedListener(response => {
    }); 

    const unsubscribe = props.navigation.addListener('focus', () => {
      if (refetch) {
        console.log(location)
        refetch();
      }
    });

    return () => {
      Notifications.removeNotificationSubscription(notificationListener.current);
      Notifications.removeNotificationSubscription(responseListener.current);
      unsubscribe;
    };
  }, []);


  if(status === 'loading' || status === 'idle'){
    return(
      <Loader 
        visible={true}
        textContent={"Procesando..."}
        textStyle={{color:"#FFF"}}
      />
    )
  }else{

    return (
        <View style={{flex: 1, }}>
          
          {
            status === 'success' &&  bookingData.data.data.request.length > 0 &&
            (
              
              <ScrollView style={{paddingHorizontal:10, marginTop:125}}>
                {
                  bookingData.data.data.request.map((item,idx)=>(
                    <>
                      <View style={{flexDirection:'row', width:'100%', justifyContent:'center', marginVertical:5}}>
                        <Text style={{color:PRIMARY_COLOR, textAlign:'center', fontSize:16}}>Solicitudes Pendientes</Text>
                      </View>
                      <ListItem key={idx} bottomDivider style={Styles.bidBox} onPress={
                        ()=> props.navigation.navigate('AceptReject',{
                          booking: item}
                        )
                      }
                      >
                        <ListItem.Content>
                          <View style={{flexDirection:'row'}}>
                            <View style={{width:'20%'}}>
                              <Avatar size={'medium'}  rounded source={{ uri:item.profilePic}} />
                            </View>
                            <View style={{width:'50%'}}>
                              <Text style={{alignSelf:'flex-start', color:"#000"}}>{item.firstName} {item.lastName}</Text>
                              <Rating  style={{alignSelf:'flex-start'}} imageSize={20} readonly startingValue={item.averageRating}  />
                            </View>
                            <View style={{width:'30%', alignItems:'flex-end'}}>
                              <Text>$ {numberFormat(item.accounting.amount)}</Text>
                              <Text style={{fontSize:12, color:PRIMARY_COLOR}}>{item.accounting.totalActualJobTimeMinutes/60} horas</Text>
                            </View>
                          </View>
                          <View style={{flexDirection:'row', width:'100%', marginVertical:4}}>
                            <Text>{item.addLine1}</Text>
                          </View>
                          <View style={{flexDirection:'row', width:'100%', marginVertical:4}}>
                            <Text style={{fontWeight:'bold', color:LIGHT_BLACK}}>Forma de Pago: </Text><Text>{item.accounting.paymentMethodText}</Text>
                          </View>

                          <View style={{flexDirection:'row', width:'100%', marginVertical:4}}>
                            <Text style={{color:LIGHT_BLACK}}>
                              {item.jobDescription}
                            </Text>
                          </View>
                          <View style={{flexDirection:'row', width:'100%', marginVertical:4, justifyContent:'flex-end'}}>
                            <Text style={{color:LIGHT_BLACK}}>
                              {toHourHumanReadable(item.eventStartTime)}
                            </Text>
                          </View>
                          
                          
                        </ListItem.Content>
                      </ListItem>
                    </>
                  ))
                }
              </ScrollView>
            )
            
          }

          <View style={[Styles.buttonsBarTop, {paddingHorizontal:10}]}>
            <View style={{width:'25%', justifyContent:'center', alignItems:'flex-start'}}>
              <TouchableOpacity
                onPress={()=> props.navigation.navigate('Notifications')}
              >
                <Fontisto 
                  name={'bell-alt'} 
                  size={38} 
                  color={PRIMARY_COLOR} 
                />
              </TouchableOpacity>
            </View>
            <View style={{width:'50%', justifyContent:'center'}}>
              <TouchableOpacity
                  style={session.status === 3 ? styles.roundGreenButton : styles.roundRedButton}
                  onPress={()=> changeStatus()}
              >
                  <Text style={[CommonStyles.whiteText,{fontSize:16}]}>
                    {session.status === 3 ? 'DESCONECTAR': 'CONECTAR'}
                  </Text>
              </TouchableOpacity>
            </View>
            <View style={{width:'25%', justifyContent:'center', alignItems:'flex-end'}}>
              <TouchableOpacity
                onPress={()=> props.navigation.navigate('ProfileStack',{
                  screen: 'Wallet'
                })}
              >
                <Ionicons name={'ios-wallet-outline'} size={38} color={PRIMARY_COLOR} />
              </TouchableOpacity>
            </View>
          </View>

          <View style={[Styles.barParentInfo, {paddingHorizontal:10}]}>
            <View style={Styles.parentInfoContainer}>
              <View style={{width:'50%', justifyContent:'center', alignItems:'center'}}>
                <Text style={{color:PRIMARY_COLOR, fontWeight:'bold', fontSize:24}}>{parents}</Text>
              </View>
              <View style={{width:'50%', justifyContent:'center', alignItems:'center'}}>
                <Text style={{color:PRIMARY_COLOR, fontWeight:'bold', fontSize:14, textAlign:'center'}}>Padres conectados actualmente</Text>
              </View>
            </View>
          </View>
        </View>
    );
  }
  
}
 
export default Home;

const styles = StyleSheet.create({
  roundGreenButton:{
    backgroundColor:GREEN,
    borderRadius:25,
    padding:10,
    alignItems:'center'
  },
  roundRedButton:{
    backgroundColor:RED_ERROR,
    borderRadius:25,
    padding:10,
    alignItems:'center'
  },
  bidBox:{
    borderWidth:1, 
    borderColor:DARK_GRAY, 
    borderRadius:5,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,

    elevation: 5
  }
})