import {StyleSheet} from 'react-native';
import { PRIMARY_COLOR } from '../../utils/Constants';

export default StyleSheet.create({
    buttonsBarTop:{
        position:'absolute', 
        top: 0, 
        width: '100%', 
        height: 60 , 
        alignSelf: 'center',
        backgroundColor:'#fff',
        flexDirection:'row',
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity:  0.4,
        shadowRadius: 3,
        elevation: 5
    },
    barParentInfo:{
        position:'absolute', 
        top: 60, 
        width: '100%', 
        height: 60 , 
        alignSelf: 'center',
        backgroundColor:'#fff',
        flexDirection:'row',
        padding:5,
        justifyContent:'center'
    },
    parentInfoContainer:{
        borderColor: PRIMARY_COLOR,
        borderWidth:2,
        flexDirection:'row',
        borderRadius:25,
        width:'80%'
    }
})