import React, { useEffect, useState, useContext, useRef } from 'react';
import {View,Text,Dimensions,Modal,Image,StyleSheet, TouchableOpacity, Alert} from 'react-native';
import StepIndicator from 'react-native-step-indicator';
import { MaterialIcons } from '@expo/vector-icons';
import { Avatar,Rating } from 'react-native-elements'
import CommonStyles from '../../utils/CommonStyles';
import Separator from '../../components/Separator';
import { numberFormat } from '../../utils/Utils';
import { Ionicons } from '@expo/vector-icons';
import { BOOKING_ARRIVED, BOOKING_JOB_TIMER_COMPLETED, BOOKING_JOB_TIMER_STARTED, BOOKING_ON_THE_WAY, DARK_GRAY, GREEN, LIGHT_BLACK, LIGHT_GRAY, PRIMARY_COLOR } from '../../utils/Constants';
import { SessionContext } from '../../context/store/Session';
import { LocationContext } from '../../context/store/Location';
import SwipeButton from 'rn-swipe-button';
import { useMutation } from 'react-query';
import { changeBookingStatus, getBookingById, providerBookingStatus } from '../../services';
import Loader from '../../components/Loader';
import MapView, { Marker } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import { ScrollView } from 'react-native-gesture-handler';
const { width, height } = Dimensions.get('window');
import CountDown from 'react-native-countdown-component';
import { useQuery } from 'react-query';

const BookingProgress = (props) => {

    const MINUTE_MS = 10000;
    const ASPECT_RATIO = width / height;
    const LATITUDE_DELTA = 0.0922;
    const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;

    const INIT_LABELS = ["Abrir Trabajo","Contratada","En Camino","Llegó","Iniciado"];
    const FINISH_LABELS = ["Abrir Trabajo","Contratada","En Camino","Llegó","Finalizado"];
    const {session} = useContext(SessionContext);
    const { reservation }  = props.route.params;
    const [booking, setBooking] = useState(reservation);
    const {location} = useContext(LocationContext);
    const [showCancel, setShowCancel] = useState(false);
    const [options, setOptions] = useState([false, false, false]);
    const [cancelReason, setCancelReason] = useState([]);
    const [selectedReason, setSelectedReason] = useState(null);
    const [loading, setLoading] = useState(false)
    const [currenStatus, setCurrentStatus] = useState(booking.status)

    const mounted = useRef(true)

    const bookingChangeMutation = useMutation((payload) => {
        return providerBookingStatus(payload);
    });



    const submitStatus = async(status) =>{
        const newStatus = status;
        setLoading(true)
        const payload = {
            bookingId: reservation.bookingId,
            status: status,
            latitude: location.latitude,
            longitude: location.longitude
        }
        const resp = await providerBookingStatus(payload)
        .then(res => {
            if(res.status == 200){
                setLoading(false)
                const newStep = newStatus === 3 ? 1 : newStatus === 6 ? 2 : 
                newStatus === 7 ? 3 :  
                (newStatus === 8 || newStatus === 9) ? 4 : 0;
                setCurrentPosition(newStep)
                setCurrentStatus(newStatus)
            }
        }).catch(err =>{
            setLoading(false)
            console.log(err.request)
        });
    }

    const [currentPosition, setCurrentPosition] = useState(
        booking.status === 3 ? 1 : booking.status === 6 ? 2 : 
        booking.status === 7 ? 3 :  
        (booking.status === 8 || booking.status === 9) ? 4 : 0
    );

    const [labels, setLabels] = useState(INIT_LABELS);

    const getStatus = () =>{
        if(currenStatus === 1){
            return 0;
        }else if(currenStatus === 3){
            return 1;
        }else if(currenStatus === 6){
            return 2;
        }else if(currenStatus === 7){
            return 3;
        }else if(currenStatus === 8 || currenStatus === 9){
            return 4;
        }
    }

    const getStepIndicatorIconConfig = ({
        position,
        stepStatus,
      }) => {
        const iconConfig = {
          name: 'feed',
          color: stepStatus === 'finished' ? '#ffffff' : '#4aae4f',
          size: 15,
        };
        switch (position) {
          case 0: {
            iconConfig.name = 'check';
            break;
          }
          case 1: {
            iconConfig.name = 'check';
            break;
          }
          case 2: {
            iconConfig.name = 'check';
            break;
          }
          case 3: {
            iconConfig.name = 'check';
            break;
          }
          case 4: {
            iconConfig.name = 'check';
            break;
          }
          default: {
            break;
          }
        }
        return iconConfig;
    };

    const renderStepIndicator = (params) => (
        <MaterialIcons {...getStepIndicatorIconConfig(params)} />
    );


    const ActionButtons = () =>(
        <View style={{flexDirection:'row',borderWidth:2, borderColor:LIGHT_GRAY,borderRadius:5}}>
            <View style={{flex:1, alignItems:'center',justifyContent:'center'}}>
                <TouchableOpacity onPress={()=> startChat() } style={{alignItems:'center'}}>
                    <Ionicons name="md-chatbubbles-outline" color={PRIMARY_COLOR} size={32} /> 
                    <Text style={{fontSize:12}}>Chat</Text>
                </TouchableOpacity>
            </View>
            {
            (
                currentPosition === 1 || currentPosition === 2) && 
                <View style={{flex:1, alignItems:'center',justifyContent:'center'}}>
                    <TouchableOpacity onPress={()=> setShowCancel(true) } style={{alignItems:'center'}}>
                        <Ionicons name="close-circle-outline" color={PRIMARY_COLOR} size={32} /> 
                        <Text style={{fontSize:12}}>Cancelar</Text>
                    </TouchableOpacity>
                </View>
            }
            <View style={{flex:1, alignItems:'center',justifyContent:'center'}}>
                <TouchableOpacity onPress={()=> 
                    props.navigation.navigate('ReservationDetails',{
                        reservation: reservation
                    })
                } style={{alignItems:'center'}}>
                    <Ionicons name="information-circle-outline" color={PRIMARY_COLOR} size={32} /> 
                    <Text style={{fontSize:12}}>Detalles</Text>
                </TouchableOpacity>
            </View>
        </View>
    )

    const bookingBaseInfo = () =>{
        return(
            <>
                <View style={[Styles.row,{justifyContent:'flex-start'}]}>
                    <Text style={CommonStyles.labelBold}> Servicio solicitado</Text>
                </View>
                <View style={[Styles.row, {marginVertical:15}]}>
                    <View style={Styles.halfColumn}>
                        <Text>Horas ${numberFormat(reservation.accounting.pricePerHour)}/hr x {reservation.accounting.totalExpectJobTimeMinutes > 0 ? reservation.accounting.totalExpectJobTimeMinutes/60 : 1}</Text>
                    </View>
                    <View style={[Styles.halfColumn,{alignItems:'flex-end'}]}>
                        <Text>${numberFormat(reservation.accounting.amount)}</Text>
                    </View>
                </View>
                <Separator />
                <View style={[Styles.row,{marginVertical:10}]}>
                    <View style={Styles.halfColumn}>
                        <Text style={CommonStyles.labelBold}>Total</Text>
                    </View>
                    <View style={[Styles.halfColumn,{alignItems:'flex-end'}]}>
                        <Text style={CommonStyles.labelBold}>${numberFormat(reservation.accounting.amount)}</Text>
                    </View>
                </View>
                <View style={[Styles.row,{marginVertical:10}]}>
                    <View style={Styles.halfColumn}>
                        <Text style={CommonStyles.labelBold}>Metodo de Pago</Text>
                    </View>
                    <View style={[Styles.halfColumn,{alignItems:'flex-end'}]}>
                        <Text style={CommonStyles.labelBold}>{reservation.accounting.paymentMethodText}</Text>
                    </View>
                </View>
            </>
        )
    }

    const clientPicture = () =>{
        return(
            <>
                <View style={Styles.row}>
                <Avatar rounded 
                    size={'large'}
                    source={{uri: reservation.profilePic}} 
                />    
                </View>
                <View style={Styles.row}>
                    <Text style={CommonStyles.titleText}>{reservation.firstName + ' ' + reservation.lastName}</Text>
                </View>
                <View style={Styles.row}>
                    <Rating  style={{alignSelf:'flex-start'}} imageSize={20} readonly startingValue={reservation.averageRating}  />

                </View>
            </>
        )
    }

    const bodyAccepted = () =>{
        return(
            <>
                {clientPicture()}
                <Separator />
                {bookingBaseInfo()}
                
                <View style={[Styles.bottom,{width:'100%', marginHorizontal:5, backgroundColor:"#fff"}]}>
                    <SwipeButton
                        disabled={false}
                        swipeSuccessThreshold={70}
                        height={40}
                        width={'100%'}
                        title="En camino"
                        titleColor="white"
                        shouldResetAfterSuccess={true}
                        onSwipeSuccess={() => {
                            submitStatus(BOOKING_ON_THE_WAY)
                        }}
                        railFillBackgroundColor={PRIMARY_COLOR}
                        railFillBorderColor={PRIMARY_COLOR}
                        thumbIconBackgroundColor="#ffffff"
                        thumbIconBorderColor="#c1c1c1"
                        railBackgroundColor={PRIMARY_COLOR}
                        railBorderColor="#c1c1c1"
                    />
                    <ActionButtons />
                </View>

                
            </>
        )
    }

    const bodyOnTheWay = () =>{
        return(
            <>
                <MapView
                    ref={map => {this.map = map}}
                    style={{height:'45%'}}
                    provider={MapView.PROVIDER_GOOGLE}
                    showsUserLocation={true}
                    showsMyLocationButton={true}
                    loadingEnabled={true}
                    initialRegion={{
                        latitude: location.latitude || 0,
                        longitude: location.longitude || 0,
                        latitudeDelta: LATITUDE_DELTA,
                        longitudeDelta: LONGITUDE_DELTA,
                    }} 
                    onMapReady={() => {
                        this.map.fitToSuppliedMarkers(['source','destiny'],{ edgePadding: 
                        {
                            top: 50,
                            right: 50,
                            bottom: 50,
                            left: 50
                        }    
                    })}}
                >
                    <MapViewDirections
                        origin={{
                            latitude: location.latitude || 0,
                            longitude: location.longitude || 0,
                        }}
                        destination={{
                            latitude: -33.407740,
                            longitude: -70.562495,
                        }}
                        apikey={"AIzaSyCL-DkMFc5UMuTIFGGHv9fNH5XQ4M5s4DY"} // insert your API Key here
                        strokeWidth={4}
                        strokeColor={PRIMARY_COLOR}
                        onStart={(params) => {
                            console.log(`Started routing between "${params.origin}" and "${params.destination}"`);
                        }}
                    />
                    
                    <Marker coordinate={{
                            latitude: location.latitude || 0,
                            longitude: location.longitude || 0
                        }}
                        pinColor={GREEN}
                        identifier={'source'}
                    />
                    <Marker coordinate={{
                            latitude: -33.407740,
                            longitude: -70.562495,
                        }}
                        identifier={'destiny'} 
                    />
                </MapView>
                <View style={{height:'30%',marginTop:15}}>
                    {clientPicture()}
                </View>
                <View style={[Styles.bottom,{width:'100%', marginHorizontal:5, backgroundColor:"#fff"}]}>
                    <SwipeButton
                        disabled={false}
                        swipeSuccessThreshold={70}
                        height={40}
                        width={'100%'}
                        title="He llegado"
                        titleColor="white"
                        shouldResetAfterSuccess={true}
                        onSwipeSuccess={() => {
                            submitStatus(BOOKING_ARRIVED)
                        }}
                        railFillBackgroundColor={PRIMARY_COLOR}
                        railFillBorderColor={PRIMARY_COLOR}
                        thumbIconBackgroundColor="#ffffff"
                        thumbIconBorderColor="#c1c1c1"
                        railBackgroundColor={PRIMARY_COLOR}
                        railBorderColor="#c1c1c1"
                    />
                    <ActionButtons />
                </View>
            </>
        )
    }

    const bodyArrived = () =>{
        return(
            <>
                {clientPicture()}
                <Separator />
                {bookingBaseInfo()}
                <View style={[Styles.row,{marginVertical:10}]}>
                    <Text>{reservation.addLine1} {reservation.addLine2}</Text>
                </View>
                
                <View style={[Styles.bottom,{width:'100%', marginHorizontal:5, backgroundColor:"#fff"}]}>
                    <SwipeButton
                        disabled={false}
                        swipeSuccessThreshold={70}
                        height={40}
                        width={'100%'}
                        title="Iniciar trabajo"
                        titleColor="white"
                        shouldResetAfterSuccess={true}
                        onSwipeSuccess={() => {
                            submitStatus(BOOKING_JOB_TIMER_STARTED)
                        }}
                        railFillBackgroundColor={PRIMARY_COLOR}
                        railFillBorderColor={PRIMARY_COLOR}
                        thumbIconBackgroundColor="#ffffff"
                        thumbIconBorderColor="#c1c1c1"
                        railBackgroundColor={PRIMARY_COLOR}
                        railBorderColor="#c1c1c1"
                    />
                </View>

                
            </>
        )
    }

    const bodyFinish = () =>{
        return(
            <>
                <View style={[Styles.row,{marginVertical:10}]}>
                    <Text style={{fontSize:16, fontWeight:'bold'}}>Tiempo restante del servicio</Text>
                </View>
                <View style={[Styles.row]}>
                    <CountDown
                        until={600}
                        size={20}
                        digitStyle={{backgroundColor: '#FFF'}}
                        digitTxtStyle={{color: PRIMARY_COLOR, borderWidth: 2, padding:5, borderColor: PRIMARY_COLOR, borderRadius:5}}
                        timeToShow={['H','M', 'S']}
                        timeLabels={{h:'Hrs', m: 'Min', s: 'Seg'}}
                        onFinish={() => {
                            Alert.alert(
                                "Aviso",
                                "Se agoto el tiempo para responder la solicitud",
                                [
                                { text: "Ok", onPress: () => 
                                    {
                                        //changeBookingStatus(BOOKING_JOB_TIMER_COMPLETED)
                                    }}
                                ]
                            );
                        }}
                    />
                </View>
                {bookingBaseInfo()}
                <View style={[Styles.row,{marginVertical:10}]}>
                    <Text>{reservation.addLine1} {reservation.addLine2}</Text>
                </View>
                
                <View style={[Styles.bottom,{width:'100%', marginHorizontal:5, backgroundColor:"#fff"}]}>
                    <SwipeButton
                        disabled={false}
                        swipeSuccessThreshold={70}
                        height={40}
                        width={'100%'}
                        title="Finalizar trabajo"
                        titleColor="white"
                        shouldResetAfterSuccess={true}
                        onSwipeSuccess={() => {
                            submitStatus(BOOKING_JOB_TIMER_COMPLETED)
                        }}
                        railFillBackgroundColor={PRIMARY_COLOR}
                        railFillBorderColor={PRIMARY_COLOR}
                        thumbIconBackgroundColor="#ffffff"
                        thumbIconBorderColor="#c1c1c1"
                        railBackgroundColor={PRIMARY_COLOR}
                        railBorderColor="#c1c1c1"
                    />
                </View>

                
            </>
        )
    }

    useEffect(()=>{
        if(currenStatus){
            setCurrentPosition(getStatus());
            if(currenStatus === 9){
                setLabels(FINISH_LABELS);
                props.navigation.navigate('Invoice',{
                    reservation: reservation
                });
            }
        }
    },[currenStatus])
    

    useEffect(()=>{
        props.navigation.setOptions({
            title: 'Trabajo ID: ' + reservation.bookingId
        });
    },[])

    if(loading){
        return(
            <Loader 
                visible={true}
                textContent={"Procesando..."}
                textStyle={{color:"#FFF"}}
            />
        )
    }else{

        return (
           
                <View style={{flex:1, backgroundColor:'#fff', padding:5}}>
                    <StepIndicator
                        currentPosition={currentPosition}
                        renderStepIndicator={renderStepIndicator}
                        labels={labels}
                    />
                    <Separator />
                    {(currentPosition === 0 || currentPosition === 1) && bodyAccepted()}
                    {currentPosition === 2 && bodyOnTheWay()}
                    {currentPosition === 3 && bodyArrived()}
                    {currentPosition === 4 && bodyFinish()}
                </View>
            
        );
    }
}
 
export default BookingProgress;

const Styles =  StyleSheet.create({
    container: {
        flex:1,
        padding:20,
        backgroundColor:'#FFF',
        alignItems:'center',
        justifyContent:'center'
    },
    row:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        
    },
    halfColumn:{
        width:'50%',
        padding:2,
        alignSelf:'center'
    },

    bottom: {
        position: 'absolute',
        bottom:5
    }
})