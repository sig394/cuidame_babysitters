import React, {useContext, useRef, useState} from 'react';
import {View, StyleSheet, Text, ScrollView, TouchableOpacity, Alert} from 'react-native'
import CommonStyles from '../../utils/CommonStyles';
import { numberFormat } from '../../utils/Utils';
import Separator from '../../components/Separator';
import SwipeButton from 'rn-swipe-button';
import { BOOKING_JOB_COMPLETED_RAISE_INVOICE, PRIMARY_COLOR } from '../../utils/Constants';
import { useMutation } from 'react-query';
import { patchPassword, providerBookingStatus } from '../../services';
import Loader from '../../components/Loader';
import { LocationContext } from '../../context/store/Location';
import SignaturePad from 'react-native-signature-pad';
import { RNS3 } from 'react-native-aws3';

const Invoice = (props) => {

    const { reservation }  = props.route.params;
    const [loading, setLoading] = useState(false)
    const {location} = useContext(LocationContext)
    const [clear, setClear] = useState(true)
    const [sign, setSign] = useState(null)

    const bookingInvoiceMutation = useMutation((payload) => {
        return providerBookingStatus(payload);
    });

    const submitStatus = () =>{
        if(sign !== null){
            setLoading(true)
            const payload = {
                bookingId: reservation.bookingId,
                status: BOOKING_JOB_COMPLETED_RAISE_INVOICE,
                latitude: location.latitude,
                longitude: location.longitude,
                signatureUrl: "",
                additionalService: []
            }
            bookingInvoiceMutation.mutate(payload, {
                onSuccess: async (data, variables, context) => {
                    Alert.alert(
                        "Aviso",
                        "El servicio ha finalizado con éxito",
                        [
                        { text: "Aceptar", onPress: () => {
                            props.navigation.navigate('Home')
                        } }
                        ]
                    );
                },
                onError: (error, variables, context) => {
                setLoading(false)
                console.log(error)
                Alert.alert("Error","No ha sido posible procesar su solicitud")
                },
            });
        }else{
            Alert.alert(
                "Aviso",
                "Debe colocar la firma para poder confirmar",
                [
                { text: "Aceptar", onPress: () => {
                    
                } }
                ]
            );
        }
    }

    if(loading){
        return(
            <Loader 
                visible={true}
                textContent={"Procesando..."}
                textStyle={{color:"#FFF"}}
            />
        )
    }else{

        return (
            <ScrollView>
                <View style={{flex:1, backgroundColor:'#fff', padding:5}}>
                    <View style={[Styles.row,{justifyContent:'flex-start'}]}>
                        <Text style={CommonStyles.labelBold}> Servicio prestado</Text>
                    </View>
                    <View style={[Styles.row, {marginVertical:15}]}>
                        <View style={Styles.halfColumn}>
                            <Text>Horas ${numberFormat(reservation.accounting.pricePerHour)}/hr x {reservation.accounting.totalExpectJobTimeMinutes > 0 ? reservation.accounting.totalExpectJobTimeMinutes/60 : 1}</Text>
                        </View>
                        <View style={[Styles.halfColumn,{alignItems:'flex-end'}]}>
                            <Text>${numberFormat(reservation.accounting.amount)}</Text>
                        </View>
                    </View>
                    <Separator />
                    <View style={[Styles.row,{marginVertical:10}]}>
                        <View style={Styles.halfColumn}>
                            <Text style={CommonStyles.labelBold}>Total</Text>
                        </View>
                        <View style={[Styles.halfColumn,{alignItems:'flex-end'}]}>
                            <Text style={CommonStyles.labelBold}>${numberFormat(reservation.accounting.amount)}</Text>
                        </View>
                    </View>
                    <View style={[Styles.row,{marginVertical:10}]}>
                        <View style={Styles.halfColumn}>
                            <Text style={CommonStyles.labelBold}>Metodo de Pago</Text>
                        </View>
                        <View style={[Styles.halfColumn,{alignItems:'flex-end'}]}>
                            <Text style={CommonStyles.labelBold}>{reservation.accounting.paymentMethodText}</Text>
                        </View>
                    </View>
                    <View style={[Styles.row,{marginVertical:10}]}>
                        <Text>{reservation.addLine1} {reservation.addLine2}</Text>
                    </View>
                    <Separator />
                    <View style={[Styles.row,{justifyContent:'flex-start'}]}>
                        <Text style={{}}>Firma del cliente</Text>
                    </View>
                    <Separator />

                    {
                        clear &&
                        <View style={{flex: 1, height:180}}>
                            <SignaturePad onError={(err)=> console.log(err)}
                                onChange={(data) => setSign(data)}
                                style={{flex: 1, backgroundColor: 'white'}}
                            />
                        </View>
                    }
                    <View style={[Styles.row,{justifyContent:'flex-start'}]}>
                        <TouchableOpacity
                            style={[CommonStyles.primaryButton,{width:'100%', marginRight:2}]}
                            onPress={() => {
                                setClear(false)
                                setSign(null)
                                setTimeout(() => {
                                    setClear(true)
                                },1000);
                            }}
                        >
                            <Text style={CommonStyles.whiteText}>Limpiar</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[Styles.row,{width:'100%', marginHorizontal:5, backgroundColor:"#fff"}]}>
                        <SwipeButton
                            disabled={false}
                            swipeSuccessThreshold={70}
                            height={40}
                            width={'100%'}
                            title="Boleta de cliente"
                            titleColor="white"
                            shouldResetAfterSuccess={true}
                            onSwipeSuccess={() => {
                                submitStatus()
                            }}
                            railFillBackgroundColor={PRIMARY_COLOR}
                            railFillBorderColor={PRIMARY_COLOR}
                            thumbIconBackgroundColor="#ffffff"
                            thumbIconBorderColor="#c1c1c1"
                            railBackgroundColor={PRIMARY_COLOR}
                            railBorderColor="#c1c1c1"
                        />
                    </View>
                </View>
            </ScrollView>
            
        );
    }
}
 
export default Invoice;

const Styles =  StyleSheet.create({
    container: {
        flex:1,
        padding:20,
        backgroundColor:'#FFF',
        alignItems:'center',
        justifyContent:'center'
    },
    row:{
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
        
    },
    halfColumn:{
        width:'50%',
        padding:2,
        alignSelf:'center'
    },

    bottom: {
        position: 'absolute',
        bottom:5
    }
})