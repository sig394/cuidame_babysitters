import React, {useContext, useEffect, useState} from 'react';
import {View,StyleSheet, Text, TouchableOpacity, Alert} from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { BOOKING_ACCEPT, BOOKING_REJECT, DARK_GRAY, LIGHT_BLACK, PRIMARY_COLOR, RED_ERROR } from '../../utils/Constants';
import { Avatar, Rating } from 'react-native-elements'
import { numberFormat, toHourHumanReadable } from '../../utils/Utils';
import Fontisto from 'react-native-vector-icons/Fontisto';
import CommonStyles from '../../utils/CommonStyles';
import CountDown from 'react-native-countdown-component';
import { useMutation } from 'react-query';
import { changeBookingStatus } from '../../services';
import { LocationContext } from '../../context/store/Location';
import Loader from '../../components/Loader';

const AceptReject = (props) => {

    const {location} = useContext(LocationContext)
    const { booking }  = props.route.params;
    const currentDate = new Date().getTime();
    const remainingTime = new Date(booking.bookingExpireTime - currentDate).getSeconds();
    const [loading, setLoading] = useState(false)

    const bookingChangeMutation = useMutation((payload) => {
        return changeBookingStatus(payload);
    });

    useEffect(()=>{
        props.navigation.setOptions({
            title: 'Trabajo ID: ' + booking.bookingId
        });
    }, [])

    const handleAcept = () =>{
        setLoading(true)
        const payload = {
            bookingId: booking.bookingId,
            status: BOOKING_ACCEPT,
            latitude: location.latitude,
            longitude: location.longitude
        }
        bookingChangeMutation.mutate(payload, {
            onSuccess: async (data, variables, context) => {
              setLoading(false) 
              Alert.alert(
                "Aviso",
                "Solicitud aceptada,una notificación ha sido enviada al cliente",
                [
                  {
                    text: "OK",
                    onPress: () => {
                        props.navigation.navigate('Home')   
                    }
                  }
                ],
                { cancelable: false }
              );
            },
            onError: (error, variables, context) => {
              setLoading(false)
              console.log(error)
              Alert.alert("Error","No ha sido posible procesar su solicitud")
            },
        });
    }

    const handleReject = () =>{
        setLoading(true)
        const payload = {
            bookingId: booking.bookingId,
            status: BOOKING_REJECT,
            latitude: location.latitude,
            longitude: location.longitude
        }
        bookingChangeMutation.mutate(payload, {
            onSuccess: async (data, variables, context) => {
              setLoading(false) 
              Alert.alert(
                "Aviso",
                "Solicitud rechazada,una notificación ha sido enviada al cliente",
                [
                  {
                    text: "OK",
                    onPress: () => {
                        props.navigation.navigate('Home')   
                    }
                  }
                ],
                { cancelable: false }
              );
            },
            onError: (error, variables, context) => {
              setLoading(false)
              console.log(error)
              Alert.alert("Error","No ha sido posible procesar su solicitud")
            },
        });

        
    }

    if(loading){
        return(
            <Loader 
                visible={true}
                textContent={"Procesando..."}
                textStyle={{color:"#FFF"}}
            />
        )
    }else{

        return (
            <View style={{flex:1, padding:10}}>
                <ScrollView>
                    
                    <View style={[Styles.col,{alignItems:'center'}]}>
                        <View style={{alignItems:'center'}}>
                            <Fontisto 
                                name={'clock'} 
                                size={24} 
                                color={DARK_GRAY} 
                            />
                            <Text style={{color:LIGHT_BLACK, fontWeight:'bold', fontSize:18}}>Tiempo restante</Text>
                        </View>
                        <CountDown
                            until={600}
                            size={30}
                            digitStyle={{backgroundColor: '#FFF'}}
                            digitTxtStyle={{color: PRIMARY_COLOR, borderWidth: 2, padding:10, borderColor: PRIMARY_COLOR, borderRadius:5}}
                            timeToShow={['M', 'S']}
                            timeLabels={{m: 'Min', s: 'Seg'}}
                            onFinish={() => {
                                Alert.alert(
                                    "Aviso",
                                    "Se agoto el tiempo para responder la solicitud",
                                    [
                                    { text: "Ok", onPress: () => props.navigation.navigate('Home')}
                                    ]
                                );
                            }}
                        />
                    </View>
                    <View style={[Styles.col,{alignItems:'center'}]}>
                        <Avatar size={'large'}  rounded source={{ uri:booking.profilePic}} />
                    </View>
                    <View style={Styles.col}>
                        <Text style={{color:LIGHT_BLACK, fontWeight:'bold', fontSize:18}}>Cliente</Text>
                        <Text style={{fontSize:16, marginVertical:5}}>{booking.firstName} {booking.lastName}</Text>
                        <Rating  style={{alignSelf:'flex-start'}} imageSize={20} readonly startingValue={booking.averageRating}  />
                    </View>

                    <View style={Styles.col}>
                        <Text style={{color:LIGHT_BLACK, fontWeight:'bold', fontSize:18}}>Dirección del servicio</Text>
                        <Text style={{fontSize:16, marginVertical:5}}>{booking.firstName} {booking.addLine1}</Text>
                        <View style={[Styles.row, {alignItems:'center'}]}>
                            <Text style={{color:LIGHT_BLACK, fontWeight:'bold', fontSize:18, width:'75%'}}>Hora  del servicio</Text>
                            <Text style={{fontSize:16, color:RED_ERROR, width:'20%'}}>{toHourHumanReadable(booking.eventStartTime)}</Text>
                        </View>
                    </View>

                    <View style={Styles.col}>
                        <Text style={{color:LIGHT_BLACK, fontWeight:'bold', fontSize:18}}>Detalles del Pago</Text>
                        {
                            booking.accounting.paidByWallet === 1 ?
                            (
                                <View style={[Styles.row,{alignItems:'center'}]}>
                                    <Fontisto 
                                        name={'wallet'} 
                                        size={24} 
                                        color={DARK_GRAY} 
                                    />
                                    <Text style={{fontSize:16,marginLeft:5}}>Pago a traves de Billletera </Text>
                                </View>
                            )
                            :
                            (
                                <View style={[Styles.row,{alignItems:'center'}]}>
                                    <Fontisto 
                                        name={'credit-card'} 
                                        size={24} 
                                        color={DARK_GRAY} 
                                    />
                                    <Text style={{fontSize:16,marginLeft:5}}>Tarjeta terminada en <Text style={{fontWeight:'bold'}}>{booking.accounting.last4}</Text></Text>
                                </View>
                            )
                        }
                        <View style={[Styles.row,{alignItems:'center', marginVertical:5}]}>
                            <Text style={{fontSize:16,marginLeft:5}}>{booking.accounting.totalActualJobTimeMinutes/60} Horas = ${numberFormat(booking.accounting.amount)}</Text>

                        </View>
                    </View>

                    <View style={Styles.col}>
                        <View style={[Styles.row]}>
                            <TouchableOpacity
                                style={[CommonStyles.acceptButton,{width:'50%', marginRight:2}]}
                                onPress={() => {
                                    Alert.alert(
                                        "Confirmación",
                                        "Esta seguro(a) de aceptar la solicitud?",
                                        [
                                        {
                                            text: "Cancelar",
                                            onPress: () => console.log("Cancel Pressed"),
                                            style: "cancel"
                                        },
                                        { text: "Aceptar", onPress: () => handleAcept() }
                                        ]
                                    );
                                }}
                            >
                                <Text style={CommonStyles.whiteText}>Aceptar</Text>
                            </TouchableOpacity>

                            <TouchableOpacity
                                style={[CommonStyles.rejectButton,{width:'50%'}]}
                                onPress={() => {
                                    Alert.alert(
                                        "Confirmación",
                                        "Esta seguro(a) de recharzar la solicitud?",
                                        [
                                        {
                                            text: "Cancelar",
                                            onPress: () => console.log("Cancel Pressed"),
                                            style: "cancel"
                                        },
                                        { text: "Rechazar", onPress: () => handleReject() }
                                        ]
                                    );
                                }}
                            >
                                <Text style={CommonStyles.whiteText}>Rechazar</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                </ScrollView>
            </View>
        );
    }
}
 
export default AceptReject;

const Styles = StyleSheet.create({
    col:{
        flexDirection:'column',
        marginVertical:5,
        backgroundColor:'#fff',
        padding: 5
    },
    row:{
        flexDirection:'row',
    }
})
