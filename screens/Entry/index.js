import React, { useContext, useEffect } from 'react';
import { ImageBackground, Text, View, Linking, TouchableOpacity, Alert } from 'react-native';
import Styles from './Styles';
import commonStyles from '../../utils/CommonStyles';
import * as Device from 'expo-device';
import * as Application from 'expo-application';
import Constants from "expo-constants"
import { DeviceContext } from '../../context/store/Device';
import * as Location from 'expo-location';

const Entry = (props) => {

  const {dispatchDevice} = useContext(DeviceContext)  
  //const {Location} = useContext(LocationContext)

  function getDevice(){
    return {
        appBuild: Application.nativeBuildVersion,
        appId: Application.applicationId,
        appName: Constants.manifest.name,
        appVersion: Constants.manifest.version,
        isVirtual: false,
        manufacturer: Device.manufacturer,
        model: Device.modelName,
        operatingSystem: Device.osName,
        osVersion: Device.osVersion,
        platform: Device.platformApiLevel,
        uuid:Device.osInternalBuildId
    }
  }

  useEffect(()=>{
    async function fetchLocation() {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
          console.log('Permission to access location was denied');
          return {};
      }
       
    }
    fetchLocation();
  },[props.navigation])

  useEffect(()=>{
    dispatchDevice({type:'SET_DEVICE', payload: getDevice()})
  },[])


  return (
    <View style={Styles.container}>
      <ImageBackground source={require('../../assets/splash.png')}
        style={Styles.background}
      >
      
        <View style={Styles.buttonsContainer}>
          <View style={Styles.row}>
            <Text style={Styles.boldCenterText}>
              Niñeras de confianza a domicilio
            </Text>
          </View>
          <View style={Styles.row}>
            <Text style={Styles.normalCenterText}>CUIDAME Babysitters APP: administra tu tiempo como quieras!</Text>
          </View>
          <View style={Styles.halfColumn}>
            <TouchableOpacity
                style={commonStyles.outlinePrimaryButton}
                onPress={()=> {
                  props.navigation.navigate('Login')
                }}
            >
                <Text style={commonStyles.primaryText}>Ingresar</Text>
            </TouchableOpacity>
          </View>
          <View style={Styles.halfColumn}>
            <TouchableOpacity
                style={commonStyles.primaryButton}
                onPress={()=> Linking.openURL('https://www.cuidame.cl/trabajo-de-ninera-paso1/')}
            >
                <Text style={commonStyles.whiteText}>Registrate</Text>
            </TouchableOpacity>
          </View>
        </View>
        
      </ImageBackground>
    </View>
  );
}
 
export default Entry;