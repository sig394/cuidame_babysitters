import {StyleSheet} from 'react-native';
import { DARK_GRAY, LIGHT_BLACK } from '../../utils/Constants';

export default StyleSheet.create({
  background:{
    width:'100%',
    height:'100%'
  },
  container:{
    flex:1,
    backgroundColor: "#FFF",
    height: '100%',
  },
  bottom:{
      flex: 1,
      width:'100%',
      position: 'absolute', 
      justifyContent: 'center',
      alignItems:'center',
      bottom: 5
  },
  buttonsContainer:{
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
    position: 'absolute', 
    bottom: 5,
  },
  row:{
    width:'100%',
  },
  halfColumn:{
    width:'50%',
    padding:5
  },
  normalCenterText:{
    textAlign:'center',
    marginVertical:15,
    marginHorizontal:10,
    fontSize: 16
  },
  boldCenterText:{
    color: LIGHT_BLACK,
    fontWeight: 'bold',
    fontSize: 20,
    textAlign:'center',
    marginVertical:15
  }
});