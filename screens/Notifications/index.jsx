import React from 'react';
import { ScrollView, Text, View } from 'react-native';
import { useQuery } from 'react-query';
import Loader from '../../components/Loader';
import { getNotifications } from '../../services';
import { PRIMARY_COLOR } from '../../utils/Constants';
import { toHumanReadable } from '../../utils/Utils';


const Notifications = () => {

    const {status, data} = useQuery('notifications', ()=> getNotifications(),{
        refetchOnWindowFocus: false
    })

    if(status === 'loading'){
        return(
            <Loader 
                visible={true}
                textContent={"Cargando..."}
                textStyle={{color:"#FFF"}}
            />
        )   
    }else{
        if(data){
            return (
                
                <ScrollView style={{paddingHorizontal:10}}>
                    
                    {
                        data.map((notif, idx)=>(
                            <View key={idx} style={{flex:1, flexDirection:'column', marginVertical:8}}>
                                <View style={{flex:1,width:'100%',paddingVertical:2}}>
                                    <Text style={{color:PRIMARY_COLOR, fontSize:16}}>{toHumanReadable(notif.timeStamp)}</Text>
                                </View>
                                <View style={{flex:1,width:'100%'}}>
                                    <Text style={{textAlign:'justify', fontSize:18}}>{notif.msg}</Text>
                                </View>

                            </View>
                            
                        ))
                    }
                </ScrollView>
            );
        }else{
            <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                <Text style={{fontWeight:'bold', fontSize:16}}>No hay notificaciones</Text>
            </View>
        }
        
    }
}
 
export default Notifications;