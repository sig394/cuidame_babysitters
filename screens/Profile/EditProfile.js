import React, {useContext, useEffect, useState} from 'react';
import {View, Text, StyleSheet,TouchableOpacity,TextInput, Alert } from 'react-native';
import CommonStyles from '../../utils/CommonStyles';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Avatar } from 'react-native-elements'
import { DARK_GRAY, PRIMARY_COLOR,AWS_S3_BUCKET, AWS_REGION, AWS_ACCESS_KEY, AWS_AUTH_SECRET } from '../../utils/Constants';
import { useForm, Controller } from "react-hook-form";
import CountryPicker from 'react-native-country-picker-modal'
import * as ImagePicker from 'expo-image-picker';
import { RNS3 } from 'react-native-aws3';
import LottieView from 'lottie-react-native';
import { useMutation } from 'react-query';
import { patchProfile } from '../../services';
import Loader from '../../components/Loader';
import { SessionContext } from '../../context/store/Session';

const EditProfile = (props) => {

  const {session, dispatchSession} = useContext(SessionContext)
  const [editable, setEditable] = useState(false)
  const [countryPhoneCode, setCountryPhoneCode] = useState('CL')
  const [countryNumber, setCountryNumber] = useState('56')
  const [phone, setPhone] = useState(session.mobile)
  const [pic, setPic] = useState(session.profilePic);
  const [profilePic, setProfilePic] = useState(null);
  const [uploading, setUploading] = useState(false);
  const [originalPic, setOriginalPic] = useState(session.profilePic);
  const [loading, setLoading] = useState(false)

  const awsOptions = {
    keyPrefix: "ProfilePics/",
    bucket: AWS_S3_BUCKET,
    region: AWS_REGION,
    accessKey: AWS_ACCESS_KEY,
    secretKey: AWS_AUTH_SECRET,
    successActionStatus: 201
  }


  const profileMutation = useMutation((payload) => {
    return patchProfile(payload);
  });

  
  const doLogout = async() =>{
    try{
        await AsyncStorage.removeItem('session');
        dispatchSession({type:'CLEAR', payload:null})
    }catch(err){
        console.log(err)
    }
  }

  const { handleSubmit, control,errors, clearErrors } = useForm({
    defaultValue:{firstName:session.firstName,lastName:session.lastName},
    mode:'onSubmit'
  });

  const handlePhoneChange = (text) =>{
    if (/^\d+$/.test(text)) {
        setPhone(text)
    }
  }

  const pickImage = async () => {
    console.log("Pick");
    let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.All,
        aspect: [4, 3],
        quality: 1,
    });
    
    if (!result.cancelled) {
        
        let ext = result.uri.substr(result.uri.lastIndexOf('.') + 1);
        let name = Math.round(+new Date()/1000);

        let file = {
            uri: result.uri,
            name:  name + "." + ext,
            type: result.type + "/" + ext
        }
        setUploading(true);
        RNS3.put(file, awsOptions).then(response => {
            setUploading(false);
            if(response.status === 201){
                let picUrl = response.body.postResponse.location;
                const urlPic = picUrl.replace('%2F','/'); 
                setProfilePic(urlPic)
                setPic(urlPic)
            }
        });
    }
  }

  const EditButton = ({title}) =>(
    <View style={{flexDirection: "row",justifyContent: "flex-end",paddingRight:10,width: 120}}>
      <TouchableOpacity onPress={()=> setEditable(!editable)}>
        <Text style={{color:PRIMARY_COLOR}}>{title}</Text>
      </TouchableOpacity> 
    </View>
  )

  const onSubmitProfile = (data) =>{
    setLoading(true)
    let payload = data;
    payload['profilePic'] = profilePic !== null ? profilePic : session.profilePic;
    payload['countryCode'] = countryNumber !== null ? countryNumber : '56';

    

    profileMutation.mutate(payload, {
      onSuccess: async (data, variables, context) => {
        setEditable(false)
        setLoading(false) 
        Alert.alert(
          "Aviso",
          "Perfil Actualizado",
          [
            {
              text: "OK",
              onPress: () => {
                console.log("OK BUTTON")
                console.log(payload)
                dispatchSession({type:'SET_PROFILE', payload:payload})            
              }
            }
          ],
          { cancelable: false }
        );
      },
      onError: (error, variables, context) => {
        setLoading(false)
        console.log(error)
        Alert.alert("Error","No ha sido posible actualizar su perfil, intente nuevamente porfavor")
      },
    });
    
  } 

  useEffect(()=>{
    props.navigation.setOptions({
      headerRight : props => <EditButton title={'Editar'} />
    });
  },[])

  useEffect(()=>{
    if(!editable){
        setPic(originalPic);
    }
    props.navigation.setOptions({
        headerRight : props => <EditButton title={editable ? 'Cancelar' : 'Editar'} />
    });
  },[editable]);

  if(loading){
    return(
      <Loader 
        visible={true}
        textContent={"Cargando..."}
        textStyle={{color:"#FFF"}}
      />
    )
  }else{

    return (
      <View style={Styles.container}>
        <View style={[Styles.row,{marginTop:5}]}>
            <View style={{width:'100%', alignItems:'center', marginBottom:15}}>
              {
                uploading ?
                  (
                    <LottieView 
                        source={require('../../assets/simple-loading-animation.json')}
                        style={{ justifyContent: "center", alignSelf: "center", height: "40%", width: "40%" }}
                        autoPlay={true}
                        loop={true}
                        speed={1}
                    />
                  )
                  :
                  (
                    <Avatar rounded 
                      size="large" 
                      containerStyle={{borderColor:PRIMARY_COLOR, borderWidth:2}}
                      source={{uri: pic}} 
                      onPress={()=> editable ? pickImage() : null}
                    />
                  )
              }
            </View>

            <View style={{width:'100%', marginVertical:5}}>
              <View style={{flexDirection:'row'}}>
                <View style={{width:'50%', flexDirection:'column'}}>
                  <Text style={{fontSize:12, color:DARK_GRAY, paddingLeft:10}}>Nombre</Text>

                  <Controller
                    control={control}
                    render={({ onChange, onBlur, value } ) => (
                      <View style={CommonStyles.inputContainer}>
                        <TextInput
                            style={CommonStyles.inputStyle}
                            onBlur={onBlur}
                            onChangeText={value => onChange(value)}
                            value={value}
                            maxLength={50}
                            editable={editable}
                        />
                      </View>
                    )}
                    name="firstName"
                    rules={{
                      required: true
                    }}
                    
                    defaultValue={session.firstName}
                  />
                </View>
                <View style={{width:'50%'}}>
                  <Text style={{fontSize:12, color:DARK_GRAY, paddingLeft:10}}>Apellido</Text>
                  <Controller
                    control={control}
                    render={({ onChange, onBlur, value } ) => (
                      <View style={CommonStyles.inputContainer}>
                        <TextInput
                            style={CommonStyles.inputStyle}
                            onBlur={onBlur}
                            onChangeText={value => onChange(value)}
                            value={value}
                            maxLength={50}
                            editable={editable}
                        />
                      </View>
                    )}
                    name="lastName"
                    rules={{
                      required: true
                    }}
                    
                    defaultValue={session.lastName}
                  />
                </View>
              </View>
            </View>

            <View style={{width:'100%', marginVertical:5}}>
              <Text style={{fontSize:12, color:DARK_GRAY, paddingLeft:10}}>Email</Text>
              <View style={CommonStyles.inputContainer}>
                <TextInput
                    style={CommonStyles.inputStyle}
                    value={session.email}
                    editable={false}
                />
              </View>
            </View>

            <View style={{width:'100%', marginVertical:5}}>
              <Text style={{fontSize:12, color:DARK_GRAY, paddingLeft:10}}>Número de teléfono</Text>
              <View style={{flexDirection:'row', paddingLeft:10, paddingTop:10}}>
                <CountryPicker 
                  containerButtonStyle={{width:'10%',paddingTop:10}}
                  countryCode={countryPhoneCode}
                  withFilter={true}
                  withFlag={true}
                  withAlphaFilter={true}
                  withCallingCode={true}
                  withEmoji={true}
                  onSelect={(country) => setCountry(country)}
                />
                <Text style={{width:'10%', paddingTop:15}}>+{countryNumber}</Text>
                
                <Controller
                    control={control}
                    render={({ onChange, onBlur, value } ) => (
                      <View style={[CommonStyles.inputContainer,{width:'75%'}]}>
                        <TextInput
                          keyboardType='numeric'
                          style={CommonStyles.inputStyle}
                          editable={editable}
                          value={phone}
                          maxLength={12}
                          onBlur={onBlur}
                          onChangeText={value => onChange(value)}
                        />
                      </View>
                    )}
                    name="phone"
                    rules={{
                      required: true
                    }}
                    defaultValue={session.mobile}
                />
              </View>
            </View>

            {
              editable ?
                <View style={{width:'100%', marginTop:20, marginBottom:5}}>
                  <TouchableOpacity
                          style={CommonStyles.primaryButton}
                          onPress={handleSubmit(onSubmitProfile)
                          }
                      >
                      <Text style={[CommonStyles.whiteText,{fontWeight:'bold', textTransform:'uppercase'}]}>Guardar</Text>
                  </TouchableOpacity>
                </View>
              :
              (
                <>
                  <View style={{width:'100%', marginTop:20, marginBottom:5}}>
                      <TouchableOpacity
                              style={CommonStyles.primaryButton}
                              onPress={()=>props.navigation.navigate('Password')}
                          >
                          <Text style={[CommonStyles.whiteText,{fontWeight:'bold', textTransform:'uppercase'}]}>Cambiar contraseña</Text>
                      </TouchableOpacity>
                  </View>

                  <View style={{width:'100%', marginVertical:5}}>
                      <TouchableOpacity
                              style={CommonStyles.outlinePrimaryButton}
                              onPress={()=>doLogout()}
                          >
                          <Text style={[CommonStyles.primaryText,{fontWeight:'bold', textTransform:'uppercase'}]}>Salir</Text>
                      </TouchableOpacity>
                  </View>
                </>
              )
            }
        </View>
      </View>
    );
  }
}
 
export default EditProfile;

const Styles = StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: "#FFF",
    padding: 20
  },
  row:{
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start',
  },
});