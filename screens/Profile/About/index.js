import React from 'react';
import {View, Text, Linking, Platform, StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Ant from 'react-native-vector-icons/AntDesign';
import { DARK_GRAY, LIGHT_BLACK, PRIMARY_COLOR } from '../../../utils/Constants';

const About = () => {
    return (
        <View style={{flex:1, backgroundColor:'#FFF', padding:20}}>
            <View style={[Styles.row,{marginBottom:10, alignItems:'center'}]}>
                <Icon name={'star-o'} size={16} color={LIGHT_BLACK} style={{marginRight:10}}/>
                {Platform.OS === 'android' ?
                    (
                        <Text style={Styles.label} onPress={()=> Linking.openURL('https://play.google.com/store/apps/details?id=com.cuidamepro')}>
                            Calificanos en Google Play
                        </Text>
                    )
                    :
                    (
                        <Text style={Styles.label} onPress={()=> Linking.openURL('https://apps.apple.com/us/app/cuidame-padres/id1592135890')}>
                            Calificanos en Apple Store
                        </Text>
                    )
                }
            </View>
            <View
                style={{
                    borderBottomColor: '#cecece',
                    borderBottomWidth: 1,
                    marginVertical:10
                }}
            />
            <View style={[Styles.row,{alignItems:'center'}]}>
                <Ant name={'like2'} size={16} color={LIGHT_BLACK} style={{marginRight:10}}/>
                <Text style={Styles.label} onPress={()=> Linking.openURL('https://www.facebook.com/cuidame.cl/')}>Danos me gusta en Facebook</Text>
            </View>
            <View style={[Styles.row,{marginTop:50, alignSelf:'center'}]}>
                <Text style={Styles.link} onPress={()=> Linking.openURL('https://cuidame.cl')}>https://www.cuidame.cl</Text>
            </View>
            <View style={[Styles.row,{marginVertical:5, alignSelf:'center'}]}>
                <Text style={Styles.link}>1.1.6</Text>
            </View>
        </View>
    );
}
 
export default About;

const Styles = StyleSheet.create({
    row:{
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    },
    label:{
        fontSize:13,
        color: DARK_GRAY
    },
    
    logoContainer: {
        height: '20%',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        
    },
    logo: {
        width: '70%',
        height: '50%',
        resizeMode:'contain'
    },
    link:{
        fontSize:14,
        color: PRIMARY_COLOR
    },
    icon: {
        width: 80,
        height: 40,
        resizeMode:'contain'
    },
    bottom:{
        flex: 1,
        justifyContent: 'flex-end',
        marginBottom: 20
    }
});