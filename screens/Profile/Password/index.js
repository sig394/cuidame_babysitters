import React, { useState} from 'react';
import { useForm, Controller } from "react-hook-form";
import { StyleSheet,View, Text, TextInput, Alert, TouchableOpacity } from 'react-native';
import Loader from '../../../components/Loader';
import CommonStyles from '../../../utils/CommonStyles';
import { useMutation } from 'react-query';
import { patchPassword } from '../../../services';
import { RED_ERROR } from '../../../utils/Constants';

const Password = (props) => {

    const [loading,setLoading] = useState(false)
    const [noMatch, setNoMatch] = useState(false)

    const { handleSubmit, control,errors,setValue, reset } = useForm({
        defaultValues:{currentPasssword:'',newPassword:'',passwordConfirm:''},
        mode:'onBlur'
    });

    const passwordMutation = useMutation((payload) => {
        return patchPassword(payload)
      });

    const showError = (msg) => {
        return (
            <Text
                style={{
                  color: RED_ERROR,
                  padding: 5,
                  paddingLeft: 12,
                  fontSize: 12
                }}
              >
                {msg}
            </Text>
        );
    }

    const onSubmit = (data) => {

        if(data.newPassword.length > 0 && data.passwordConfirm.length > 0){
            if(data.newPassword !== data.passwordConfirm){
                setNoMatch(true)
            }else{
                setNoMatch(false)
                const payload = {
                    newPassword: data.newPassword,
                    oldPassword: data.currentPasssword
                }
                setLoading(true)
                console.log(payload)

                passwordMutation.mutate(payload, {
                    onSuccess: async (data, variables, context) => {
                      
                      setLoading(false) 
                      Alert.alert(
                        "Aviso",
                        "Contraseña Actualizada",
                        [
                          {
                            text: "OK",
                            onPress: () => {
                              props.navigation.goBack();
                            }
                          }
                        ],
                        { cancelable: false }
                      );
                    },
                    onError: (error, variables, context) => {
                      setLoading(false)
                      console.log(error)
                      Alert.alert("Error","No ha sido posible actualizar su contraseña, intente nuevamente porfavor")
                    },
                });
            }
        }else{
            setNoMatch(false)
        }

        

        
    }

    if(loading){
        return(
            <Loader 
                visible={true}
                textContent={"Cargando..."}
                textStyle={{color:"#FFF"}}
            />
        )
    }else{

        return (
            <View style={Styles.editContainer}>
                <View style={Styles.row}>
                    <Controller
                        control={control}
                        render={({ onChange, onBlur, value }) => (
                            <View style={CommonStyles.inputContainer}>
                                <TextInput
                                    style={CommonStyles.inputStyle}
                                    placeholder={"Contraseña Actual"}
                                    onBlur={onBlur}
                                    onChangeText={value => onChange(value)}
                                    value={value}
                                    secureTextEntry={true}
                                    
                                />
                            </View>
                        )}
                        name="currentPasssword"
                        rules={{ required: true }}
                        defaultValue=""
                    />
                </View>

                <View style={Styles.row}>
                    <Controller
                        control={control}
                        render={({ onChange, onBlur, value }) => (
                            <View style={CommonStyles.inputContainer}>
                                
                                <TextInput
                                    style={CommonStyles.inputStyle}
                                    placeholder={"Contraseña Nueva"}
                                    onBlur={onBlur}
                                    onChangeText={value => onChange(value)}
                                    value={value}
                                    secureTextEntry={true}
                                    
                                />
                            </View>
                        )}
                        name="newPassword"
                        rules={{ required: true }}
                        defaultValue=""
                    />
                    {errors.newPassword && showError("Campo requerido")}
                    {noMatch && showError("Contraseñas no coinciden")}
                </View>
                <View style={Styles.row}>
                    <Controller
                        control={control}
                        render={({ onChange, onBlur, value }) => (
                            <View style={CommonStyles.inputContainer}>
                                
                                <TextInput
                                    style={CommonStyles.inputStyle}
                                    placeholder={"Confirmar Contraseña"}
                                    onBlur={onBlur}
                                    onChangeText={value => onChange(value)}
                                    value={value}
                                    secureTextEntry={true}
                                    
                                />
                            </View>
                        )}
                        name="passwordConfirm"
                        rules={{ required: true }}
                        defaultValue=""
                    />
                    {errors.passwordConfirm && showError("Campo requerido")}
                    {noMatch && showError("Contraseñas no coinciden")}
                </View>

                <View style={{marginTop:20}}>
                    <TouchableOpacity
                        style={CommonStyles.primaryButton}
                        onPress={handleSubmit(onSubmit)}
                    >
                        <Text style={[CommonStyles.whiteText,{fontWeight:'bold', textTransform:'uppercase'}]}>Cambiar contraseña</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
 
export default Password;

const Styles = StyleSheet.create({
    editContainer: {
        flex:1,
        padding:20,
        backgroundColor:'#FFF'
    },
    row:{
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'flex-start',
    }
})
