import React, { useEffect, useState } from 'react';
import { ScrollView, Text, View } from 'react-native';
import { useQuery } from 'react-query';
import Loader from '../../../components/Loader';
import { getReviews } from '../../../services';
import { GRAY, PRIMARY_COLOR } from '../../../utils/Constants';
import { Rating as Rate,ListItem,Avatar } from 'react-native-elements'
import { toHumanReadable } from '../../../utils/Utils';

const Rating = (props) => {

    const [rating, setRating] = useState(0);

    const {status, data} = useQuery('rating', ()=> getReviews(),{
        refetchOnWindowFocus: false
    })

    useEffect(()=>{
        if(data){
            let total = 0;
            data.data.data.reviews.map((r, idx)=>{
                total += r.rating
            })
            total = (total /data.data.data.reviews.length).toFixed(2); 
            setRating(total)
        }
    },[data])

    if(status === 'loading'){
        return(
            <Loader 
                visible={true}
                textContent={"Cargando..."}
                textStyle={{color:"#FFF"}}
            />
        )   
    }else{
        console.log(data.data)
        return(
            <View style={{flex:1, backgroundColor:'#fff', padding:10}}>
                <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                    <Text style={{color:PRIMARY_COLOR, fontSize:22, fontWeight:'bold'}}>{rating}</Text>
                    <Rate imageSize={24} startingValue={rating}  style={{marginLeft:10}} readonly />
                </View>
                <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                    <Text>Promedio de calificaciones </Text>
                    <Text>| {data.data.data.reviews.length} opiniones</Text>
                </View>
                <View style={{borderTopWidth:1, borderColor:GRAY,width:'100%', marginVertical:10}} />
                
                {
                    data.data.data.reviews.length > 0 ?
                    (
                        
                        data.data.data.reviews.map((item, i) => (
                            <>
                            <View style={{flexDirection:'row',alignItems:'center', justifyContent:'center', marginVertical:15}}>
                                <View style={{width:'15%'}}>
                                    <Avatar rounded 
                                        size="medium" 
                                        source={{uri: item.profilePic}} 
                                    />
                                </View>
                                <View style={{width:'55%', alignItems:'flex-start', justifyContent:'center'}}>
                                    <View style={{flexDirection:'column'}}>
                                        <Text>{item.reviewBy}</Text>
                                        <Text>{toHumanReadable(item.reviewAt)}</Text>
                                    </View>
                                </View>
                                <View style={{width:'20%', justifyContent:'flex-end'}}>
                                    <Rate imageSize={22} startingValue={item.rating}  style={{marginLeft:10}} readonly />
                                </View>
                            </View>
                            <View style={{borderTopWidth:1, borderColor:GRAY,width:'100%'}} />
                            </>
                        ))
                        
                    )
                    :
                    (
                        <View style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                            <Text style={{fontWeight:'bold', fontSize:16}}>No cuenta con calificaciones</Text>
                        </View>
                    )
                }
            </View>
        )
    }
}
 
export default Rating;