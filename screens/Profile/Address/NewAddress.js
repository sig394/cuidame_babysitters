import React, {useContext, useEffect, useRef, useState} from 'react';
import {View, Dimensions, StyleSheet, TextInput,Text, TouchableOpacity,Modal,Alert} from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import { LocationContext } from '../../../context/store/Location';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import { GRAY, PRIMARY_COLOR, GOOGLE_MAP_API_KEY } from '../../../utils/Constants';
import { Header } from 'react-native-elements';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { useMutation } from 'react-query';
import { saveAddress } from '../../../services';
import Loader from '../../../components/Loader';
import * as Location from 'expo-location';

const { width, height } = Dimensions.get('window');
const ASPECT_RATIO = width / height;
const LATITUDE_DELTA = 0.0922;
const LONGITUDE_DELTA = LATITUDE_DELTA * ASPECT_RATIO;


const NewAddress = (props) => {

    
    const searchRef = useRef();
    const [tag, setTag] = useState('home')
    const [modalVisible, setModalVisible] = useState(false)
    const [address ,setAddress] = useState(null)
    const [loading, setLoading] = useState(false)
    const [currentLoc, setCurrentLoc] = useState(null);
    
    const addressMutation = useMutation((payload) => {
        return saveAddress(payload)
      });

    const RadioTouch = ({label, value}) => (
        <View style={{flex:1,flexDirection:'row', alignItems:'center'}}>
            <TouchableOpacity 
                style={{borderRadius:25, padding:10, backgroundColor:tag === value ? PRIMARY_COLOR :'#fff', borderColor:PRIMARY_COLOR, borderWidth:2, marginHorizontal:5}}    
                onPress={() => setTag(value)}
            />
            <Text style={{fontSize:20}}>{label}</Text>
        </View>

    )

    const submitAddress = () =>{
        setLoading(true)
        addressMutation.mutate(address, {
            onSuccess: async (data, variables, context) => {
                setLoading(false)
              Alert.alert(
                "Aviso",
                "Dirección guardada",
                [
                  {
                    text: "OK",
                    onPress: () => {
                      props.navigation.navigate('Address')
                    }
                  }
                ],
                { cancelable: false }
              );
            },
            onError: (error, variables, context) => {
              setLoading(false)
              console.log(error)
              Alert.alert("Error","No ha sido posible guardar la dirección, intente nuevamente porfavor")
            },
        });
    }

    const SaveButton = () =>(
        <View style={{flexDirection: "row",justifyContent: "flex-end",paddingRight:10,width: 120}}>
          <TouchableOpacity onPress={()=> submitAddress()}>
            <Text style={{color:PRIMARY_COLOR, fontSize:20}}>Guardar</Text>
          </TouchableOpacity> 
        </View>
    )
    
    useEffect(()=>{
        console.log("aca")
        async function getCurrentLoc(){
            try {
              let { status } = await Location.requestForegroundPermissionsAsync();
              if (status !== 'granted') {
                  console.log('Permission to access location was denied');
                  return {};
              }
              let location = await Location.getCurrentPositionAsync({enableHighAccuracy:true});
              setCurrentLoc(location.coords)
            }
            catch(e){
              console.log(e)
            }
          }
          getCurrentLoc();
    },[props.navigation])

    useEffect(()=>{
        if(address){
            props.navigation.setOptions({
                headerRight : props => <SaveButton />
            });
        }
    },[address])

    if(loading || currentLoc===null){
        return(
            <Loader 
                visible={true}
                textContent={"Procesando..."}
                textStyle={{color:"#FFF"}}
            />
        )
    }else{

        return (
            <View style={{flex:1,justifyContent: 'center'}}>
                
                    <MapView
                            key={address ? address.placeId : '1'}
                            style={{flex:1}}
                            provider={MapView.PROVIDER_GOOGLE}
                            showsUserLocation={true}
                            showsMyLocationButton={false}
                            loadingEnabled={true}
                            initialRegion={{
                                latitude: address ? address.latitude : currentLoc.latitude,
                                longitude: address ? address.longitude : currentLoc.longitude,
                                latitudeDelta: LATITUDE_DELTA,
                                longitudeDelta: LONGITUDE_DELTA,
                            }} 
                            
                    >
                        <Marker
                            key={address ? address.placeId : '1'}
                            coordinate={{
                                latitude:address ? address.latitude : currentLoc.latitude,
                                longitude: address ? address.longitude : currentLoc.longitude
                            }}
                        />
                    </MapView>
                    
                    
                

                <View style={[Styles.inputSearchTop, {paddingHorizontal:10, alignItems:'center'}]}>
                    <FontAwesome name={'search'} size={28} color={PRIMARY_COLOR} />
                    <TextInput 
                        ref={searchRef}
                        style={{paddingLeft:15, fontSize:18}}
                        placeholder='Buscar'
                        onFocus={()=> {
                            setModalVisible(true)
                            searchRef.current.blur()
                        }}
                    />
                </View>
                <View style={[Styles.addressBox, {paddingHorizontal:10, alignItems:'center', justifyContent:'center'}]}>
                    <View style={{flexDirection:'row', width:'100%', marginVertical:10}}>
                        <RadioTouch label={'Casa'} value={'home'} />
                        <RadioTouch label={'Oficina'} value={'office'} />
                        <RadioTouch label={'Otro'} value={'other'} />
                    </View>
                    <View style={{borderTopWidth:2, borderTopColor:GRAY, width:'90%', marginVertical:10}} />
                    {
                        address ? <Text style={{marginVertical:10, fontSize:16}}>{address.addLine1}</Text> : null
                    }
                    
                </View>

                <Modal
                    animationType="fade"
                    transparent={false}
                    visible={modalVisible}
                    onRequestClose={() => {
                        searchRef.current.blur()
                    }}>
                        <Header
                            backgroundColor={PRIMARY_COLOR}
                            centerComponent={<Text style={{color:"#fff",fontSize:16}}>Buscar ubicacion</Text>}
                            rightComponent={{ icon: 'cancel', color: '#fff', onPress: () => {
                                searchRef.current.blur()
                                setModalVisible(false)}
                            }}
                        />
                        <View style={{ height:'100%', backgroundColor:'white', padding:10}}>
                            <GooglePlacesAutocomplete 
                                placeholder='Buscar'
                                fetchDetails={true}
                                styles={{
                                    textInput:{
                                        fontSize:22
                                    },
                                    textInputContainer:{
                                        borderBottomWidth:2,
                                        borderBottomColor: GRAY
                                    }
                                }}
                                query={{
                                    key: GOOGLE_MAP_API_KEY,
                                    language: 'es',
                                }}
                                onPress={(data, details)=> {
                                    
                                    searchRef.current.blur()

                                    const payload = {
                                        name: tag,
                                        addLine1:data.description,
                                        addLine2 : '',
                                        city:'',
                                        state: '',
                                        country: 'Chile',
                                        placeId: data.place_id,
                                        pincode: '',
                                        latitude: details.geometry.location.lat,
                                        longitude: details.geometry.location.lng,
                                        taggedAs: tag,
                                        userType: 2
                                    }

                                    setAddress(payload)
                                    setModalVisible(false)
                                    
                                }}
                                onFail={(error)=> console.log(error)}
                            />
                        </View>
                </Modal>
                
            </View>
        );
    }
}
 
export default NewAddress;

const Styles = StyleSheet.create({
    inputSearchTop:{
        position:'absolute', 
        top: 5, 
        width: '80%', 
        height: 50 , 
        alignSelf: 'center',
        backgroundColor:'#fff',
        flexDirection:'row',
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity:  0.4,
        shadowRadius: 3,
        elevation: 5,
        flexDirection:'row',
        borderRadius:10
    },
    addressBox:{
        position:'absolute', 
        top: 60, 
        width: '80%', 
        height: 'auto', 
        alignSelf: 'center',
        backgroundColor:'#fff',
        shadowColor: '#000',
        shadowOffset: { width: 1, height: 1 },
        shadowOpacity:  0.4,
        shadowRadius: 3,
        elevation: 5,
        borderRadius:10
    }


})