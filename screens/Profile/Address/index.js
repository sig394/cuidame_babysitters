import React, { useEffect, useState } from 'react';
import {ScrollView, Text, TouchableOpacity, View,Alert} from 'react-native'
import { useQuery } from 'react-query';
import Loader from '../../../components/Loader';
import { deleteAddress, getAddress } from '../../../services';
import { PRIMARY_COLOR } from '../../../utils/Constants';
import Ionicons from 'react-native-vector-icons/Ionicons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import { useMutation } from 'react-query';

const Address = (props) => {

    const [loading, setLoading] = useState(false)
    
    const {status, data, refetch} = useQuery('address', ()=> getAddress(),{})

    useEffect(()=>{
        const unsubscribe = props.navigation.addListener('focus', () => {
            if (refetch) {
              refetch();
            }
        });
        
        return unsubscribe;
    },[])

    const deleteMutation = useMutation((id) => {
        return deleteAddress(id)
      });

    const delAddress = (id) =>{
        setLoading(true)
        deleteMutation.mutate(id, {
            onSuccess: async (data, variables, context) => {
              setLoading(false) 
              Alert.alert(
                "Aviso",
                "La dirección ha sido eliminada",
                [
                  {
                    text: "OK",
                    onPress: () => {
                      refetch()
                    }
                  }
                ],
                { cancelable: false }
              );
            },
            onError: (error, variables, context) => {
              setLoading(false)
              console.log(error)
              Alert.alert("Error","No ha sido posible eliminar la dirección, intente nuevamente porfavor")
            },
        });
    }

    if(loading){
        return(
            <Loader 
                visible={true}
                textContent={"Cargando..."}
                textStyle={{color:"#FFF"}}
            />
        )   
    }else{
        
        if(status === 'loading' || status === 'idle'){
            return(
                <Loader 
                    visible={true}
                    textContent={"Cargando..."}
                    textStyle={{color:"#FFF"}}
                />
            )   
        }else{
			
            return(
                
                <View style={{flex:1, backgroundColor:'#fff', padding:10}}>
                    {

                        data &&
                        <ScrollView>
                            {
								
                                data.data.data.map((d, idx)=>(
                                    <>
                                        <View key={idx} style={{flexDirection:'row', alignItems:'center'}}>
                                            {
                                                d.taggedAs.toLowerCase() === 'home' &&
                                                <Ionicons name={'home'} size={28} style={{ width:'15%'}}/>
                                            }
                                            {
                                                d.taggedAs.toLowerCase() === 'casa' &&
                                                <Ionicons name={'home'} size={28} style={{ width:'15%'}}/>
                                            }
                                            {
                                                d.taggedAs.toLowerCase() === 'office'  &&
                                                <FontAwesome name={'building-o'} size={28} style={{ width:'15%'}} />
                                            }
                                            {
                                                d.taggedAs.toLowerCase() === 'other' &&
                                                <Feather name={'map-pin'} size={28} style={{ width:'15%'}} />
                                            }
                                            <View style={{flexDirection:'column', width:'70%'}}>
                                                <Text style={{fontWeight:'bold'}}>{d.taggedAs}</Text>        
                                                <Text>{d.addLine1}</Text>        
                                            </View>
                                            <View style={{flexDirection:'row', justifyContent:'center', width:'15%'}}>
                                                <FontAwesome name={'trash-o'} size={24} onPress={()=> delAddress(d._id)} />
                                            </View>
                                        </View>
                                        
                                    </>
                                ))
                            }
                        </ScrollView>
                    }

                    <TouchableOpacity
                        style={{padding:15, backgroundColor:PRIMARY_COLOR, borderRadius:25, alignItems:'center', marginBottom: 10}}
                        onPress={()=> props.navigation.navigate('NewAddress')}
                    >
                        <Text style={{color:"#fff"}}>AGREGAR NUEVA DIRECCIÓN</Text>
                    </TouchableOpacity>
                </View>
            )
        }
    }
}
 
export default Address;