import React, { useContext, useEffect } from 'react';
import {View, Share, ScrollView,Text} from 'react-native';
import { SessionContext } from '../../context/store/Session';
import { ListItem,Avatar } from 'react-native-elements'
import Icon from 'react-native-vector-icons/FontAwesome5';
import { APP_MARKET_URI, LIGHT_BLACK, PRIMARY_COLOR, SHARE_MSG } from '../../utils/Constants';
import * as WebBrowser from 'expo-web-browser';

const Profile = (props) => {

  const {session} = useContext(SessionContext)
  
  const options = [
    /*{
        icon:'credit-card',
        title: 'Método de Pago',
        screen: 'Cards',
        isScreen: true,
        isShare: false
    },*/
    {
        icon:'list-ul',
        title: 'Mi Tarifa Babysitter',
        isScreen: true,
        screen: 'MyRate',
        isShare: false
    },
    {
        icon:'money-check-alt',
        title: 'Mis Ganancias',
        isScreen: false,
        url:'https://admin.cuidame.cl/payrollform/?'+session.id+'&ES',
        isShare: false
    },
    {
      icon:'edit',
      title: 'Mis Direcciones',
      isScreen: true,
      screen: 'Address'
    },
    /*{
      icon:'folder-open',
      title: 'Mi Documentación Personal',
      isScreen: true,
      screen: 'Docs'
    },*/
    {
      icon:'star',
      title: 'Mis Calificaciones',
      isScreen: true,
      screen: 'Rating'
    },
    {
      icon:'question-circle',
      title: 'Chat Soporte CUIDAME',
      isScreen: false,
      url:'https://tawk.to/chat/5d940279db28311764d6cdc1/default?__cf_chl_jschl_tk__=2628c6350f951b81e4269353ce1a4c740da564ad-1618205119-0-Ab0CEjhCBVt0hM5AbxpKFoPErrDHIPnJ1g-40wBb57GBamLKu-_g-M-BTV7uudlGGA8U7RJJPzFfzt2gfFpe0mtAiKgoRoS4E0NHFm_9et0VBHx2huUUyv5Q--2u-qM0EmmpZdtnmXARNGJ_Fi0Uh5Dqi0w6QG4cS_3iFq8-StXE9r2ZeXHM-UMpOB17c_BcGfeN3KvAdqmm52BAMUOc98wzYMJy6a3aPUNvscbHtWDC_y55NhjMhzoFIerwi8oaOozqcR3TXN92nMswQgpQZaTv_aU3OjI6Uzlvk_Ns4YIqYCh03fqaeM6aqR9H4bPALyI_NXPyRo5yiYmdXuxBGLWUA4AnxJA_VPgWXtZ2CmEKFeAk6OVweLX_x7ndpVBorDBWI-64Tw8vMBGPvCkO-YZAAieZBMEqhPMP6hnRfZws',
      isShare: false
    },
    {
      icon:'comments',
      title: 'Blog CUIDAME',
      isScreen: false,
      url:'https://cuidame.cl/blog',
      isShare: false
    },
    {
        icon:'share-alt',
        title: 'Compartir',
        isScreen: false,
        isShare: true
    },
    {
        icon:'life-ring',
        title: 'Sobre CUIDAME',
        screen: 'About',
        isShare: false,
        isScreen: true
        
    },
    
    /*{
        icon:'globe',
        title: 'Seleccione el idioma',
        isScreen: true,
        isShare: false
    },*/
  ];

  

  useEffect(()=>{
    
  }, []);

  return (
    <View style={{flex:1, backgroundColor:'#FFF'}}>
      
      <ScrollView>

        
        <View style={{flexDirection:'row', alignItems:'center',justifyContent:'center', flexWrap:'wrap', marginVertical:10}}>
          <Avatar rounded 
            size="large" 
            source={{uri: session.profilePic}} 
            onPress={()=> props.navigation.navigate('EditProfile')}
          />
          
        </View>
        <View style={{flexDirection:'row', alignItems:'center',justifyContent:'center', flexWrap:'wrap', marginVertical:10}}>
          <Text style={{color:PRIMARY_COLOR, fontSize:16}}
            onPress={()=> props.navigation.navigate('EditProfile')}
          >{session.firstName} {session.lastName}</Text>
        </View>

        {
            options.map((item, i) => (
                <ListItem key={i} bottomDivider onPress={()=> {
                    if(item.isScreen){
                        props.navigation.navigate(item.screen)
                    }else if(item.isShare){
                        Share.share({
                            message:
                            `${SHARE_MSG+APP_MARKET_URI}`,
                        })    
                    }else{
                        //Linking.openURL(item.url);
                        WebBrowser.openBrowserAsync(item.url);
                    }}
                }>
                    <Icon name={item.icon} size={16} color={LIGHT_BLACK} />
                    <ListItem.Content>
                        <ListItem.Title style={{color:LIGHT_BLACK}}>{item.title}</ListItem.Title>
                    </ListItem.Content>
                    <ListItem.Chevron color={LIGHT_BLACK} />
                </ListItem>
            ))
        }
      </ScrollView>
    </View>
  );
}
 
export default Profile;