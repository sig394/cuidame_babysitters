import React, { useEffect, useState } from 'react';
import {ScrollView, Text, TouchableOpacity, View, StyleSheet, Alert} from 'react-native'
import BigInput from '../../../components/BigInput';
import Sepatator from '../../../components/Separator';
import CommonStyles from '../../../utils/CommonStyles';
import { DARK_GRAY, GREEN, LIGHT_BLACK, LIGHT_GRAY, PRIMARY_COLOR } from '../../../utils/Constants';
import { Slider } from 'react-native-range-slider-expo';
import { AntDesign } from '@expo/vector-icons';
import { useQuery } from 'react-query';
import { useMutation } from 'react-query';
import { getProviderCategory, saveRate } from '../../../services';
import SnackBar from 'react-native-snackbar-component'

const MyRate = () => {

    const min = 5000;
    const max = 7000;
    const [valRate, setValRate] = useState(6000)
    const [earn, setEarn] = useState(null)
    const [hourSel, setHourSel] = useState(3)
    const [saving, setSaving] = useState(false)
    const [showSnack, setShowSnack] = useState(false)
    const [currentRate, setCurrentRate] = useState(null)

    const {status, data} = useQuery('profile', ()=> getProviderCategory(),{})

    useEffect(()=>{
        if(data){
            if(data.data.data.length > 0){
                setValRate(parseInt(data.data.data[0].price_per_fees))
            }
        }
    },[data])
    
    const rateMutation = useMutation((payload) => {
        return saveRate(payload)
    });

    const handleSaveRate = () =>{
        setSaving(true)
        const payload = {
            'service_category_Id': '5d23491698fbca4c66257da7',
            'categoryType':1,
            'price': valRate.toString().replace(".","")
        };
        rateMutation.mutate(payload, {
            onSuccess: async (data, variables, context) => {
                setSaving(false)
                setShowSnack(true)
            },
            onError: (error, variables, context) => {
                setSaving(false)
                console.log(error)
                Alert.alert("Error","No ha sido posible actualizar su tarifa, por favor intentelo de nuevo")
            },
        });
    }

    const HourBox = ({value}) => (
        <TouchableOpacity 
            style={hourSel === value ? Styles.selHourBox : Styles.hourBox}
            onPress={()=> {
                console.log(value)
                setHourSel(value)
            }}>
            <Text style={hourSel === value ? Styles.textSelHourBox : Styles.textHourBox}>{`${value}H`}</Text>
        </TouchableOpacity>
    )

    const HourControl = () =>(
        <View style={Styles.hourControl}>
            <TouchableOpacity 
                style={{marginHorizontal:5}}
                onPress={()=> {
                    if(parseInt(hourSel) > 3){
                        setHourSel(parseInt(hourSel) - 1)
                    }
                }}
            >
                <AntDesign name="minus" size={24} />
            </TouchableOpacity>
            <Text style={{fontSize:22, textTransform:'uppercase',marginHorizontal:5}}>{`${hourSel}H`}</Text>
            <TouchableOpacity 
                style={{marginHorizontal:5}}
                onPress={()=> setHourSel(parseInt(hourSel) + 1)}
            >
                <AntDesign name="plus" size={24} />
            </TouchableOpacity>
        </View>
    )

    if(status === 'loading' || status === 'idle'){
        return null;
    }else{
        return (
            <View style={{flex:1, alignItems:'center', paddingHorizontal:10}}>
                <SnackBar 
                    visible={showSnack} 
                    textMessage="Tarifa actualizada" 
                    backgroundColor={DARK_GRAY}
                    messageColor={"#FFF"}
                    actionText="OK"
                    actionHandler={()=> setShowSnack(false)}
                />
                <ScrollView>
                    <View style={{flexDirection:'row', width:'100%', marginVertical:5}}>
                        <Text style={{textAlign:'justify', fontSize:16}}>Aqui debes seleccionar tu tarifa por hora, tienes que considerar que al precio que pague el cliente, se le descontara la comision de CUIDAME y los costos por servicio</Text>
                    </View>
                    <Sepatator />
                    <View style={{flexDirection:'row', width:'100%', marginVertical:10, alignItems:'center', justifyContent:'center'}}>
                        <Text style={{textAlign:'center', fontSize:16, fontWeight:'700', color:DARK_GRAY}}>
                            SELECCIONA TU TARIFA POR HORA
                        </Text>
                    </View>

                    <View style={{flexDirection:'row', width:'100%', marginVertical:5, alignItems:'center', justifyContent:'center'}}>
                        <BigInput 
                            prefix={'$'} 
                            inValue={valRate} 
                            onValueChange={(val)=> setValRate(parseInt(val))} 
                            min={min}
                            max={max}
                        />
                    </View>

                    <View style={{flexDirection:'row', width:'100%', marginVertical:5, alignItems:'center', justifyContent:'center'}}>
                        <Text style={{textAlign:'center', fontSize:16, fontWeight:'700', color:DARK_GRAY}}>
                            SELECCIONA TU RANGO DE PRECIOS
                        </Text>
                    </View>
                    <View style={{flexDirection:'row', width:'100%', marginVertical:5, alignItems:'center', justifyContent:'center'}}>
                        <Slider min={min} max={max} step={100}
                            valueOnChange={value => setValRate(value)}
                            initialValue={valRate}
                            knobColor={PRIMARY_COLOR}
                            valueLabelsBackgroundColor='black'
                            inRangeBarColor={DARK_GRAY}
                            outOfRangeBarColor={LIGHT_BLACK}
                        />
                    </View>

                    <View style={{flexDirection:'row', width:'100%', marginVertical:10}}>
                        <TouchableOpacity
                            disabled={saving}
                            style={[CommonStyles.primaryButton,{width:'100%'}]}
                            onPress={()=> handleSaveRate()}
                        >
                            <Text style={CommonStyles.whiteText}>GUARDAR</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{flexDirection:'row', width:'100%', marginVertical:5}}>
                        <Text style={{textAlign:'justify', fontSize:16}}>Este es el valor que pagaran los padres a traves de la app, para ver cuanto recibiras, utiliza el simulador a continuacion</Text>
                    </View>

                    <View style={{flexDirection:'row', width:'100%', padding:10, marginVertical:5, backgroundColor:PRIMARY_COLOR, justifyContent:'center'}}>
                        <Text style={{color:"#fff", fontWeight:'bold'}}>SIMULA TU GANANCIA</Text>
                    </View>

                    <View style={{flexDirection:'row', width:'100%', marginVertical:5, justifyContent:'center'}}>
                        <Text style={{textAlign:'justify', fontSize:18}}>
                            ¿Cuántas horas trabajarás al mes?
                        </Text>
                    </View>

                    <View style={{flexDirection:'row', marginVertical:5, justifyContent:'center'}}>
                        <HourBox value={3} />
                        <HourBox value={8} />
                        <HourControl />
                    </View>

                    <View style={{flexDirection:'row', width:'100%', marginVertical:10}}>
                        <TouchableOpacity
                            style={[CommonStyles.primaryButton,{width:'100%'}]}
                            onPress={()=> {
                                let value = valRate * hourSel;
                                let formatValue = value.toString().replace(".","").replace(/\B(?=(\d{3})+(?!\d))/g, ".")
                                setEarn(formatValue)
                            }}
                        >
                            <Text style={CommonStyles.whiteText}>CALCULAR</Text>
                        </TouchableOpacity>
                    </View>

                    {
                        earn &&
                        <>
                            <View style={{flexDirection:'row', width:'100%', marginVertical:5, justifyContent:'center'}}>
                                <Text style={{textAlign:'justify', fontSize:18}}>
                                    Esto ganaras
                                </Text>
                            </View>

                            <View style={{flexDirection:'row', width:'100%', marginVertical:10, justifyContent:'center', marginVertical:10}}>
                                <Text style={Styles.earnText}>{`$ ${earn}`}</Text>
                            </View>
                        </>
                    }
                    

                </ScrollView>
            </View>
        );
    }
}
 
export default MyRate;

const Styles = StyleSheet.create({
    hourBox: {
        borderRadius:10, 
        borderColor:PRIMARY_COLOR, 
        borderWidth:2, 
        padding:15, 
        marginHorizontal:8,
        backgroundColor:"#fff"
    },
    textHourBox:{
        color: PRIMARY_COLOR,
        fontSize:18
    },
    selHourBox:{
        borderRadius:10, 
        borderColor:PRIMARY_COLOR, 
        borderWidth:2, 
        padding:15, 
        marginHorizontal:8,
        backgroundColor:PRIMARY_COLOR
    },
    textSelHourBox:{
        color: "#fff",
        fontSize:18
    },
    hourControl:{
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        borderColor:PRIMARY_COLOR,
        borderRadius:10, 
        padding:15,
        borderColor:PRIMARY_COLOR,
        borderWidth:2
    },
    earnText:{
        fontSize:32,
        color: DARK_GRAY,
        
    }
});