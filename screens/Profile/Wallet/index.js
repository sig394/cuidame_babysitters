import React from 'react';
import { ScrollView, Text, TouchableOpacity, View } from 'react-native';
import { useQuery } from 'react-query';
import Loader from '../../../components/Loader';
import { getWalletData } from '../../../services';
import { DARK_GRAY, LIGHT_GRAY, PRIMARY_COLOR } from '../../../utils/Constants';
import { numberFormat } from '../../../utils/Utils';


const Wallet = (props) => {

    const {status, data} = useQuery('wallet-data', ()=> getWalletData())

    if(status === 'loading'){
        return(
            <Loader 
                visible={true}
                textContent={"Cargando..."}
                textStyle={{color:"#FFF"}}
            />
        )   
    }else{
        return (
            <View style={{flex:1, backgroundColor:'#fff'}}>
                <View style={{backgroundColor:LIGHT_GRAY,  alignItems:'center',paddingVertical:10}}>
                    <View style={{marginTop:10, alignItems:'center'}}>
                        <Text style={{fontSize:16, textTransform:'uppercase'}}>Balance Actual</Text>
                        <Text style={{fontSize:28, fontWeight:'bold'}}>{parseInt(numberFormat(data.data.data.walletAmount))}</Text>
                    </View>
                    <View style={{borderTopWidth:2, borderColor:DARK_GRAY, width:'70%',marginVertical:10}} />
                    <TouchableOpacity
                        onPress={()=> props.navigation.navigate('Transactions')}
                        style={{borderWidth:1, borderColor:PRIMARY_COLOR, padding:10, borderRadius:5, backgroundColor:'#fff'}}
                    >
                        <Text style={{textTransform:'uppercase',color:PRIMARY_COLOR}}>Transacciones Recientes</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
    
}
 
export default Wallet;