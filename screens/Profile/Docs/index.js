import React, {useState} from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Image } from 'react-native';
import { useQuery } from 'react-query';
import Loader from '../../../components/Loader';
import { getProviderProfile } from '../../../services';
import { PRIMARY_COLOR,AWS_S3_BUCKET, AWS_REGION, AWS_ACCESS_KEY, AWS_AUTH_SECRET } from '../../../utils/Constants';
import anverso from '../../../assets/rut-anverso.png'
import reverso from '../../../assets/rut-reverso.png'
import * as ImagePicker from 'expo-image-picker';
import { RNS3 } from 'react-native-aws3';
import CommonStyles from '../../../utils/CommonStyles';

const awsOptions = {
    keyPrefix: "Provider/Documents/",
    bucket: AWS_S3_BUCKET,
    region: AWS_REGION,
    accessKey: AWS_ACCESS_KEY,
    secretKey: AWS_AUTH_SECRET,
    successActionStatus: 201
}

const Docs = () => {

    const [picAnverso, setPicAnverso] = useState(null)
    const [picReverso, setPicReverso] = useState(null)

    const pickImage = async (picType) => {
        console.log("Pick");
        let result = await ImagePicker.launchImageLibraryAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            aspect: [4, 3],
            quality: 1,
        });
        
        if (!result.cancelled) {
            
            let ext = result.uri.substr(result.uri.lastIndexOf('.') + 1);
            let name = Math.round(+new Date()/1000);

            let file = {
                uri: result.uri,
                name:  name + "." + ext,
                type: result.type + "/" + ext
            }
            
            RNS3.put(file, awsOptions).then(response => {
                
                if(response.status === 201){
                    let picUrl = response.body.postResponse.location;
                    const urlPic = picUrl.replace('%2F','/'); 
                    if(picType === 'anverso'){
                        console.log(urlPic)
                        setPicAnverso(urlPic)
                    }else{
                        setPicReverso(urlPic)
                    }        
                }
            });
        }
    }

    return (
        <>
            <View style={{flex:1, alignItems:'center', paddingHorizontal:10}}>
                <View style={{flexDirection:'row', width:'100%', marginVertical:5, justifyContent:'center'}}>
                    <Text style={{color:PRIMARY_COLOR}}>FOTO CARNET ANVERSO</Text>
                </View>

                <View style={{flexDirection:'row', width:'100%', marginVertical:5, justifyContent:'center', padding:8}}>
                    <TouchableOpacity onPress={()=> pickImage('anverso')}>
                        {picAnverso ?
                            (
                                <Image style={{width:320, height:200 }} source={{uri: picAnverso}} resizeMode={'contain'}  />
                            )
                            :
                            (
                                <Image source={anverso} resizeMode={'contain'} />
                            )
                        }
                    </TouchableOpacity>
                </View>

                <View style={{flexDirection:'row', width:'100%', marginVertical:5, justifyContent:'center'}}>
                    <Text style={{color:PRIMARY_COLOR}}>FOTO CARNET REVERSO</Text>
                </View>

                <View style={{flexDirection:'row', width:'100%', marginVertical:5, justifyContent:'center', padding:8}}>
                    <TouchableOpacity onPress={()=> pickImage('reverso')}>
                        {picReverso ?
                            (
                                <Image style={{width:320, height:200 }} source={{uri: picReverso}} resizeMode={'contain'}  />
                            )
                            :
                            (
                                <Image source={reverso} resizeMode={'contain'} />
                            )
                        }
                    </TouchableOpacity>
                </View>

                
            </View>
            <View style={{flexDirection:'row', width:'100%', paddingHorizontal:5}}>
                <TouchableOpacity
                    style={[CommonStyles.primaryButton,{width:'100%', position: 'absolute', bottom:5, left:5}]}
                    onPress={()=> console.log("Salvar")}
                >
                    <Text style={CommonStyles.whiteText}>GUARDAR</Text>
                </TouchableOpacity>
            </View>
        </>
    );
}
 
export default Docs;

const Styles = StyleSheet.create({
    carnet:{
        borderRadius:5,
        borderColor:PRIMARY_COLOR,
        borderWidth:2,
        padding:10,
        width:'100%',
        height: 150
    }
});