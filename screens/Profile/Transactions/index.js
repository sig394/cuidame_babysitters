import React, {useState} from 'react';
import { View, Text,useWindowDimensions, TouchableOpacity } from 'react-native';
import { TabView, SceneMap, TabBar } from 'react-native-tab-view';
import { GREEN, LIGHT_GRAY, PRIMARY_COLOR, RED_ERROR } from '../../../utils/Constants';
import { useQuery } from 'react-query';
import { getWalletTransactions } from '../../../services';
import Loader from '../../../components/Loader';
import { numberFormat, toLongDate } from '../../../utils/Utils';
import { ScrollView } from 'react-native-gesture-handler';

const Transactions = () => {

    const layout = useWindowDimensions();
    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'all', title: 'TODAS' },
        { key: 'debit', title: 'DEBITO' },
        { key: 'credit', title: 'CREDITO' },
    ]);

    const {status, data} = useQuery('wallet-transactions', ()=> getWalletTransactions())

    const Divisor = () =>(
        <View style={{borderTopWidth:2, borderColor:LIGHT_GRAY, width:'100%',marginVertical:10}}></View>
    )

    const GreenDot = () =>(
        <TouchableOpacity style={{borderRadius:50, backgroundColor:GREEN, padding:5, width:20}} />
    )

    const RedDot = () =>(
        <TouchableOpacity style={{borderRadius:50, backgroundColor:RED_ERROR, padding:5, width:20}} />
    )

    const Card = ({data}) =>(
        <View style={{
            margin:5,
            borderWidth:2,
            borderColor:LIGHT_GRAY,
            backgroundColor:'#fff',
            borderRadius:10,
            shadowColor: '#000',
            shadowOffset: {
                width: 0,
                height: 1,
            },
            shadowOpacity: 0.22,
            shadowRadius: 2.22,
            elevation: 3,
            padding: 10,
        }}>
            <Text style={{fontSize:12}}>Trans ID: {data.txnId}</Text>
            <Divisor />
            <View style={{flexDirection:'row'}}>
                <View style={{width:'50%', alignItems:'flex-start'}}>
                    <View style={{flexDirection:'row'}}>
                        {data.txnType === 'CREDIT' ? <GreenDot /> : <RedDot /> }
                        <Text style={{marginLeft:10, fontSize:15}}>{data.trigger}</Text>
                    </View>
                </View>
                <View style={{width:'50%', alignItems:'flex-end'}}>
                    <Text style={{fontWeight:'bold'}}>{numberFormat(data.amount)}</Text>
                </View>
            </View>
            <Divisor />
            <View style={{flexDirection:'row'}}>
                <View style={{width:'50%', alignItems:'flex-start'}}>
                    <Text style={{fontSize:12}}>{toLongDate(data.timestamp)}</Text>
                </View>
                <View style={{width:'50%', alignItems:'flex-end'}}>
                    <Text style={{fontSize:12}}>Booking ID:{data.tripId}</Text>
                </View>
            </View>
        </View>
    )
    
    //creditDebitArr
    const AllRoute = () => {
        const all = data.data.data.creditDebitArr;
        return(
            <View style={{flex:1, backgroundColor:'#fff'}}>
                <ScrollView>
                    {all.map((item, idx)=>(
                        <Card data={item} />
                    ))}
                </ScrollView>
            </View>            
        )
    }
    //debitArr
    const DebitRoute = () => {
        const debit = data.data.data.debitArr;
        return(
            <View style={{flex:1, backgroundColor:'#fff'}}>
                <ScrollView>
                    {debit.map((item, idx)=>(
                        <Card data={item} />
                    ))}
                </ScrollView>
            </View>            
        )
    }
    //creditArr
    const CreditRoute = () => {
        const credit = data.data.data.creditArr;   
        return(
            <View style={{flex:1, backgroundColor:'#fff'}}>
                <ScrollView>
                    {credit.map((item, idx)=>(
                        <Card data={item} />
                    ))}
                </ScrollView>
            </View>            
        )
    }


    if(status === 'loading'){
        return(
            <Loader 
                visible={true}
                textContent={"Cargando..."}
                textStyle={{color:"#FFF"}}
            />
        )   
    }else{
        console.log(data.data)
        return(
            <TabView
                navigationState={{ index, routes }}
                renderScene={SceneMap({
                    all: AllRoute,
                    debit: DebitRoute,
                    credit: CreditRoute,
                })}
                onIndexChange={setIndex}
                initialLayout={{ width: layout.width }}
                renderTabBar={props => 
                    <TabBar {...props} 
                    style={{backgroundColor: '#fff'}}
                    renderLabel={({route, color}) => (
                        <Text style={{ color: PRIMARY_COLOR}}>
                        {route.title}
                        </Text>
                    )}
                    indicatorStyle={{backgroundColor:PRIMARY_COLOR}}
                />}
            />
        )
    }
}
 
export default Transactions;