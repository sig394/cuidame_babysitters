import React, { useContext, useState } from 'react';
import { View, Text, TextInput,Alert,TouchableOpacity } from 'react-native';
import Styles from './Styles';
import { useForm, Controller } from "react-hook-form";
import { RED_ERROR } from '../../utils/Constants';
import CommonStyles from '../../utils/CommonStyles';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useMutation } from 'react-query';
import axiosInstance from '../../api/AxiosInstance';
import Loader from '../../components/Loader';
import { SessionContext } from '../../context/store/Session';
import { DeviceContext } from '../../context/store/Device';

const Login = () => {

  const {dispatchSession} = useContext(SessionContext)
  const {device} = useContext(DeviceContext)  

  const { handleSubmit, control,errors, clearErrors } = useForm({
    defaultValue:{email:'',password:''},
    mode:'onSubmit'
  });

  const [loading, setLoading] = useState(false);
  const [secure, setSecure] = useState(true);

  const loginMutation = useMutation((payload) => {
    try{
      return axiosInstance.post("/provider/signIn",payload);
    }catch(err){
      console.log(err)
    }
  });

  const showError = (msg) => {
    return (
        <Text
            style={{
              color: RED_ERROR,
              padding: 5,
              paddingLeft: 12,
              fontSize: 12
            }}
          >
            {msg}
        </Text>
    );
  }


  const onSubmit = async(data) => {
    clearErrors();
    let devType = device.operatingSystem !== '' ? device.operatingSystem === 'android' ? 2 : 1 : 3;
    let postData = {
        mobileOrEmail: data.email,
        password: data.password,
        deviceType: devType,
        deviceId:device.uuid || '',
        pushToken: '',
        appVersion: device.appVersion || '1.0',
        deviceMake: device.manufacturer || '',
        deviceModel: device.model || '',
        deviceOsVersion: device.osVersion,
        deviceTime: new Date().getTime()
    };

    let formData = new FormData();
    Object.keys(postData).forEach((key, index)=>{
      formData.append(key,postData[key])
    })
    
    loginMutation.mutate(formData, {
      onSuccess: async (data, variables, context) => {
        const res = data.data.data;
        res['hasLogin'] = true;
        console.log("Auth:Success");
        dispatchSession({type:'SET_SESSION',payload: res});
        try {
          await AsyncStorage.setItem('session',JSON.stringify(res));
        }catch (error) {
            console.log("Error storing session data");
        }
      },
      onError: (error, variables, context) => {
        Alert.alert('Credenciales incorrectas')
        console.log(error)
      },
    });
  }


  return (
    <View style={Styles.container}>
      <Loader 
        visible={loading}
        textContent={"Cargando..."}
        textStyle={{color:"#FFF"}}
      />

      <Controller
          control={control}
          render={({ onChange, onBlur, value } ) => (
              <View style={CommonStyles.inputContainer}>
                  <TextInput
                      style={CommonStyles.inputStyle}
                      placeholder={"Tu email o teléfono"}
                      onBlur={onBlur}
                      onChangeText={value => onChange(value)}
                      value={value}
                  />
              </View>
          )}
          name="email"
          rules={{
            required: true,
            pattern: {
              value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
            }
          }}
          defaultValue={""}
          
      />
      {errors.email?.type === "required" && showError("Email requerido")}
      {errors.email?.type === "pattern" && showError("Email invalido")}

      <Controller
          control={control}
          render={({ onChange, onBlur, value })  => (
              <View style={CommonStyles.inputContainer}>
                  
                  <TextInput
                      style={CommonStyles.inputStyle}
                      placeholder={"Contraseña"}
                      onBlur={onBlur}
                      onChangeText={value => onChange(value)}
                      value={value}
                      secureTextEntry={secure}
                      
                  />
                  {/*<Icon name={secure ? 'eye-off' : 'eye'}  onPress={()=>setSecure(!secure)} />*/}
              </View>
          )}
          name="password"
          rules={{ required: true }}
          defaultValue={""}
      />
      {errors.password && showError("Contraseña requerida")}
      

      <View style={{marginTop:20}}>
        <TouchableOpacity
            style={CommonStyles.primaryButton}
            onPress={handleSubmit(onSubmit)}
        >
            <Text style={CommonStyles.whiteText}>Ingresar</Text>
        </TouchableOpacity>
      </View>
      <View style={Styles.bottomText}>
        <Text style={Styles.footer}>
            No tienes una cuenta!
        </Text>
        <Text style={CommonStyles.primaryText} onPress={()=> Linking.openURL('https://www.cuidame.cl/trabajo-de-ninera-paso1/')}>
            Registrate
        </Text>
      </View>
      
    </View>
  );
}
 
export default Login;