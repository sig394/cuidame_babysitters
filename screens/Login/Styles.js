import {StyleSheet} from 'react-native';
import { DARK_GRAY } from '../../utils/Constants';

export default StyleSheet.create({
  container: {
    flex:1,
    backgroundColor: "#FFF",
    height: '100%',
    padding: 5
  },
  bottomText:{
      flex: 1,
      width:'100%',
      position: 'absolute', 
      justifyContent: 'center',
      alignItems:'center',
      bottom: 5
  },
  footer:{
      color:DARK_GRAY,
      padding:5,
  }
});