import React, { useContext, useEffect } from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import HomeNavigation from './HomeNavigation';
import Entry from '../Entry';
import Login from '../Login';
import { SessionContext } from '../../context/store/Session';

const Stack = createStackNavigator();

const AuthNavigation = (props) => {

  const {session} = useContext(SessionContext)

    
  const InitialNavigation = () => { 
    if(session && session.hasLogin){
      return(
        <Stack.Navigator>
          <Stack.Screen
            name="HomeNavigation"
            component={HomeNavigation}
            options={{ 
              headerShown: false,
            }}
          />
        </Stack.Navigator>
      )
    }else{
      return(
        <Stack.Navigator>
          <Stack.Screen
            name="Entry"
            component={Entry}
            options={{
              headerShown: false
            }}
          />
          <Stack.Screen
            name="Login"
            component={Login}
            options={

                { 
                    title:"Ingresar",
                    headerShown: true,
                    headerStyle: {
                    backgroundColor: '#fff'
                    },
                    headerTitleAlign: "center",
                    headerTintColor: "#000",
                    headerStyle: {
                        elevation: 0,
                        shadowOpacity: 0
                    }
                }
            }
          />
        </Stack.Navigator>
        
      )
    }
  }

  return (
    <NavigationContainer independent={true}
        >
        <InitialNavigation />
      </NavigationContainer>
  );
}
 
export default AuthNavigation;