import React, {useEffect, useState} from 'react';
import { MaterialCommunityIcons, Ionicons,FontAwesome } from '@expo/vector-icons';
import { NavigationContainer, useNavigationState } from '@react-navigation/native';
import { createStackNavigator,HeaderBackButton } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { PRIMARY_COLOR } from '../../utils/Constants';
import Home from '../Home';
import Jobs from '../Jobs';
import Chat from '../Chat';
import Schedule from '../Schedule';
import Profile from '../Profile/Profile';
import EditProfile from '../Profile/EditProfile';
import Notifications from '../Notifications';
import Wallet from '../Profile/Wallet';
import Transactions from '../Profile/Transactions';
import Password from '../Profile/Password';
import About from '../Profile/About';
import Rating from '../Profile/Rating';
import Address from '../Profile/Address';
import MyRate from '../Profile/MyRate';
import NewAddress from '../Profile/Address/NewAddress';
import Docs from '../Profile/Docs';
import AceptReject from '../Booking/AceptReject';
import BookingProgress from '../Booking/BookingProgress';
import Invoice from '../Booking/Invoice';


const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

function HomeStack() {
  return(
    <Stack.Navigator
      initialRouteName="Home"
      >
      <Stack.Screen
        name="Home"
        component={Home}
        options={{ headerShown: false }}
      />

      <Stack.Screen
        name="Notifications"
        component={Notifications}
        options={
          { 
              title:"Notificaciones",
              headerShown: true,
              headerStyle: {
                  backgroundColor: '#fff'
              },
              headerTitleAlign: "center",
              headerTintColor: "#000",
              headerStyle: {
                  elevation: 0,
                  shadowOpacity: 0
              }
          }
        }
      /> 

      <Stack.Screen
        name="AceptReject"
        component={AceptReject}
        options={
    
            { 
                title:"",
                headerShown: true,
                headerStyle: {
                    backgroundColor: '#fff'
                },
                headerTitleAlign: "center",
                headerTintColor: "#000",
                headerStyle: {
                    elevation: 0,
                    shadowOpacity: 0
                },
                headerTitleStyle:{
                    fontSize: 16
                },
                headerBackTitleVisible:false
            }
        }
      />


    </Stack.Navigator>
  )
}

function JobsStack({navigation}) {
  return(
    <Stack.Navigator
      initialRouteName="Jobs"
      >
      <Stack.Screen
        name="Jobs"
        component={Jobs}
        options={
          { 
              title:"Solicitudes",
              headerShown: true,
              headerStyle: {
                  backgroundColor: '#fff'
              },
              headerTitleAlign: "center",
              headerTintColor: "#000",
              headerTitleStyle:{
                  fontSize:16
              },
              headerStyle: {
                  elevation: 0,
                  shadowOpacity: 0
              },
              headerBackTitleVisible:false,
              headerLeft : props => <HeaderBackButton onPress={()=>{
                navigation.navigate('Home');
              }} />
          }
      }
      /> 

      <Stack.Screen
        name="BookingProgress"
        component={BookingProgress}
        options={
    
            { 
                title:"",
                headerShown: true,
                headerStyle: {
                    backgroundColor: '#fff'
                },
                headerTitleAlign: "center",
                headerTintColor: "#000",
                headerTitleStyle:{
                    fontSize:16
                },
                headerStyle: {
                    elevation: 0,
                    shadowOpacity: 0
                },
                headerBackTitleVisible:false
            }
        }
        />

      <Stack.Screen
        name="Invoice"
        component={Invoice}
        options={
    
            { 
                title:"Confirmación",
                headerShown: true,
                headerStyle: {
                    backgroundColor: '#fff'
                },
                headerTitleAlign: "center",
                headerTintColor: "#000",
                headerTitleStyle:{
                    fontSize:16
                },
                headerStyle: {
                    elevation: 0,
                    shadowOpacity: 0
                },
                headerBackTitleVisible:false
            }
        }
        />
    </Stack.Navigator>

    
  )
}

function ChatStack() {
  return(
    <Stack.Navigator
      initialRouteName="Chat"
      >
      <Stack.Screen
        name="Chat"
        component={Chat}
        options={{ headerShown: false }}
      /> 
    </Stack.Navigator>
  )
}

function ScheduleStack() {
  return(
    <Stack.Navigator
      initialRouteName="Schedule"
      >
      <Stack.Screen
        name="Schedule"
        component={Schedule}
        options={{ headerShown: false }}
      /> 
    </Stack.Navigator>
  )
}

function ProfileStack({navigation}){
  return(
    <Stack.Navigator
      initialRouteName="Profile"
      >
      <Stack.Screen
        name="Profile"
        component={Profile}
        options={{ headerShown: false }}
      /> 
      <Stack.Screen
        name="EditProfile"
        component={EditProfile}
        options={
          { 
              title:"Tu Perfil",
              headerShown: true,
              headerStyle: {
                  backgroundColor: '#fff'
              },
              headerTitleAlign: "center",
              headerTintColor: "#000",
              headerStyle: {
                  elevation: 0,
                  shadowOpacity: 0
              },
              headerBackTitleVisible:false
          }
        }
      />

      <Stack.Screen
        name="Password"
        component={Password}
        options={
          { 
              title:"Cambiar Contraseña",
              headerShown: true,
              headerStyle: {
                  backgroundColor: '#fff'
              },
              headerTitleAlign: "center",
              headerTintColor: "#000",
              headerStyle: {
                  elevation: 0,
                  shadowOpacity: 0
              },
              headerBackTitleVisible:false
          }
        }
      />

      <Stack.Screen
        name="Wallet"
        component={Wallet}
        options={
          { 
              title:"Billetera",
              headerShown: true,
              headerStyle: {
                  backgroundColor: '#fff'
              },
              headerTitleAlign: "center",
              headerTintColor: "#000",
              headerStyle: {
                  elevation: 0,
                  shadowOpacity: 0
              },
              headerBackTitleVisible:false,
              headerLeft : props => <HeaderBackButton onPress={()=>{
                navigation.goBack();
              }} />
          }
        }
      />

      <Stack.Screen
        name="Transactions"
        component={Transactions}
        options={
          { 
              title:"Transacciones Recientes",
              headerShown: true,
              headerStyle: {
                  backgroundColor: '#fff'
              },
              headerTitleAlign: "center",
              headerTintColor: "#000",
              headerStyle: {
                  elevation: 0,
                  shadowOpacity: 0
              },
              headerBackTitleVisible:false,
              headerLeft : props => <HeaderBackButton onPress={()=>{
                navigation.goBack();
              }} />
          }
        }
      />

      <Stack.Screen
        name="About"
        component={About}
        options={
          { 
              title:"CUIDAME",
              headerShown: true,
              headerStyle: {
                  backgroundColor: '#fff'
              },
              headerTitleAlign: "center",
              headerTintColor: "#000",
              headerStyle: {
                  elevation: 0,
                  shadowOpacity: 0
              },
              headerBackTitleVisible:false,
              
          }
        }
      />

      <Stack.Screen
        name="Rating"
        component={Rating}
        options={
          { 
              title:"Opiniones",
              headerShown: true,
              headerStyle: {
                  backgroundColor: '#fff'
              },
              headerTitleAlign: "center",
              headerTintColor: "#000",
              headerStyle: {
                  elevation: 0,
                  shadowOpacity: 0
              },
              headerBackTitleVisible:false,
              
          }
        }
      />

      <Stack.Screen
        name="Docs"
        component={Docs}
        options={
          { 
              title:"Mis Documentos",
              headerShown: true,
              headerStyle: {
                  backgroundColor: '#fff'
              },
              headerTitleAlign: "center",
              headerTintColor: "#000",
              headerStyle: {
                  elevation: 0,
                  shadowOpacity: 0
              },
              headerBackTitleVisible:false,
              
          }
        }
      />

      <Stack.Screen
        name="Address"
        component={Address}
        options={
          { 
              title:"Mis Direcciones",
              headerShown: true,
              headerStyle: {
                  backgroundColor: '#fff'
              },
              headerTitleAlign: "center",
              headerTintColor: "#000",
              headerStyle: {
                  elevation: 0,
                  shadowOpacity: 0
              },
              headerBackTitleVisible:false,
              
          }
        }
      />

      <Stack.Screen
        name="NewAddress"
        component={NewAddress}
        options={
          { 
              title:"Tu Dirección",
              headerShown: true,
              headerStyle: {
                  backgroundColor: '#fff'
              },
              headerTitleAlign: "center",
              headerTintColor: "#000",
              headerStyle: {
                  elevation: 0,
                  shadowOpacity: 0
              },
              headerBackTitleVisible:false,
              
          }
        }
      />

      <Stack.Screen
        name="MyRate"
        component={MyRate}
        options={
          { 
              title:"Mi Tarifa",
              headerShown: true,
              headerStyle: {
                  backgroundColor: '#fff'
              },
              headerTitleAlign: "center",
              headerTintColor: "#000",
              headerStyle: {
                  elevation: 0,
                  shadowOpacity: 0
              },
              headerBackTitleVisible:false,
              
          }
        }
      />

      
    </Stack.Navigator>
  )
}

const HomeNavigation = (props) => {
    
  const isTabBarVisible = (route) => {
    const routeName = route.state
      ? route.state.routes[route.state.index].name
      : (route.params ? route.params.screen : 'HomeScreen');
    return ![
      'EditProfile',
      'Wallet',
      'Transactions',
      'Password',
      'Rating',
      'Address',
      'NewAddress',
      'MyRate',
      'Docs',
      'AceptReject',
      'Jobs',
      'BookingProgress',
      'Invoice',
    ].includes(routeName);
  };

  return(
    <NavigationContainer independent={true}>
      <Tab.Navigator
        initialRouteName="Home"
        tabBarOptions={{
            activeTintColor: PRIMARY_COLOR,
        }}>
        <Tab.Screen
          name="HomeStack"
          component={HomeStack}
          options={({ route }) => ({
              tabBarLabel: "Inicio",
              tabBarIcon: ({ color, size }) => (
                  <MaterialCommunityIcons name="home" color={color} size={32} />
              ),
              tabBarVisible: isTabBarVisible(route)
          })}
        />
        <Tab.Screen
          name="JobsStack"
          component={JobsStack}
          options={({ route }) => ({
              tabBarLabel: "Mis Trabajos",
              tabBarIcon: ({ color, size }) => (
              <Ionicons name="briefcase-outline" color={color} size={32} />
              ),
              tabBarVisible: isTabBarVisible(route)
          })}
        />
        <Tab.Screen
          name="ChatStack"
          component={ChatStack}
          options={{
              tabBarLabel: "Chat",
              tabBarIcon: ({ color, size }) => (
              <Ionicons name="md-chatbubbles-outline" color={color} size={32} />
              ),
          }}
        />
        {/*<Tab.Screen
          name="ReservationStack"
          component={ScheduleStack}
          options={{
              tabBarLabel: "Agenda",
              tabBarIcon: ({ color, size }) => (
              <FontAwesome name="calendar-check-o" color={color} size={30} />
              ),
          }}
        />*/}
        <Tab.Screen
          name="ProfileStack"
          component={ProfileStack}
          options={({ route }) => ({
              tabBarLabel: "Perfil",
              tabBarIcon: ({ color, size }) => (
              <MaterialCommunityIcons name="account" color={color} size={32} />
              ),
              tabBarVisible: isTabBarVisible(route)
          })}
        />
      </Tab.Navigator>
    </NavigationContainer>
  )
}

export default HomeNavigation;