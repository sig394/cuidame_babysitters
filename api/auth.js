import axios from 'axios';
import { API_URL } from '../utils/Constants';

const API = axios.create();

export const doLogin = async (postData, onSuccess, onError) => {
    console.log("Auth:doLogin");
    console.log(`${API_URL}provider/signIn`);
    let options = {
        url: `${API_URL}provider/signIn`,
        headers: {
            'Content-Type':'application/json'
        },
        method: 'POST'
    };

    options.data = new FormData();
    for (let key in postData) {
        options.data.append(key,postData[key]);
    }
    console.log("Sending POST Auth")
    

    const query = await API(options)
    .then(res => {
        if(res.status == 200){
            console.log(res.data.data)
            onSuccess(res.data.data)
        }
    }).catch(err =>{
        console.log("Error")
        onError(err)

    });
}