import axios from "axios";
import AsyncStorage from '@react-native-async-storage/async-storage';

const axiosInstance = axios.create({
    baseURL:'https://api.cuidame.cl',
})

axiosInstance.interceptors.request.use(async(config)=>{
    try{      
        console.log("Interceptor")
        const sessionStore = await AsyncStorage.getItem('session')
        const session = sessionStore ? JSON.parse(sessionStore) : null;
        console.log(session.token)
        if(session && session.hasLogin){
            console.log("Inject token")
            config.headers.common['Authorization'] = `Bearer ${session.token}`
        }else{
            console.log("Token no needed")
        }
    }catch(err){

    }
    return config;
})

export default axiosInstance;