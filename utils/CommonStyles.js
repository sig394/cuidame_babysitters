import {StyleSheet} from 'react-native';
import { PRIMARY_COLOR, FACE_BLUE_COLOR, GOOGLE_RED_COLOR, LIGHT_GRAY, RED_ERROR, DARK_GRAY, PRIMARY_DISABLED_COLOR, GREEN } from './Constants';

export default StyleSheet.create({
  primaryButton:{
    backgroundColor:PRIMARY_COLOR,
    borderRadius:25,
    padding:10,
    alignItems:'center'
  },

  acceptButton:{
    backgroundColor:GREEN,
    borderRadius:25,
    padding:10,
    alignItems:'center'
  },
  rejectButton:{
    backgroundColor:RED_ERROR,
    borderRadius:25,
    padding:10,
    alignItems:'center'
  },
  roundPrimaryButton:{
    backgroundColor:PRIMARY_COLOR,
    borderRadius:25,
    padding:10,
    alignItems:'center'
  },

  outlinePrimaryButton:{
    backgroundColor:'#FFF',
    borderWidth:2,
    borderColor:PRIMARY_COLOR,
    borderRadius:25,
    padding:10,
    alignItems:'center'
  },

  whiteText:{
    color:'#FFF',
    fontSize:16
  },
  primaryText:{
    color:PRIMARY_COLOR,
    fontSize:16
  },

  inputContainer: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#cecece',
    paddingBottom: 10,
    paddingHorizontal:10,
    marginHorizontal:10,
    marginVertical:10
  },
  inputContainerNoBorder:{
      flexDirection: 'row',
      borderColor: '#cecece',
      paddingBottom: 10,
      marginHorizontal:10,
      marginVertical:10
  },
  inputStyle: {
      flex: 1,
      fontSize:16
  }
});