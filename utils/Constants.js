export const API_URL = 'https://api.cuidame.cl/';

//Variables colors
export const PRIMARY_COLOR = '#9d60d7';
export const PRIMARY_DISABLED_COLOR = '#b69bd7';
export const FACE_BLUE_COLOR = '#3473E7';
export const GOOGLE_RED_COLOR = '#EA4335';
export const RED_ERROR = '#ff0000';
export const DARK_GRAY = '#808080';
export const LIGHT_GRAY = '#f7f7f7'
export const LIGHT_BLACK = '#495057';
export const GRAY = '#cecece';
export const GREEN = '#46d246';

//AWS
export const AWS_ACCESS_KEY = 'AKIAY6B6G3V5C77SYSU4'
export const AWS_AUTH_SECRET = '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'
export const AWS_REGION = 'us-east-1'
export const AWS_S3_BUCKET = 'cuidame'

//Share message
export const SHARE_MSG = 'Hola!! Te recomiendo CUIDAME babysitters para que encuentres a tu niñera ideal!!!\nDescargala aqui '
export const APP_MARKET_URI = 'market://details?id=com.cuidamepro'

//google
export const GOOGLE_MAP_API_KEY='AIzaSyCL-DkMFc5UMuTIFGGHv9fNH5XQ4M5s4DY'

//Docs
export const CARNET_ANVERSO='5d23491698fbca4c66257dac'
export const CARNET_REVERSO='5d23491698fbca4c66257dad'

//booking status
export const BOOKING_REJECT=4
export const BOOKING_ACCEPT=3
export const BOOKING_ON_THE_WAY=6
export const BOOKING_ARRIVED=7
export const BOOKING_JOB_TIMER_STARTED=8
export const BOOKING_JOB_TIMER_COMPLETED=9
export const BOOKING_JOB_COMPLETED_RAISE_INVOICE=10