import moment from "moment";
import 'moment/locale/es';

export function numberFormat(number){
    return parseInt(number).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

export function getDocumentValue(data,id){
  let doc = {}
  for(let i=0;i<data.length;i++){
    if(data[i].fId === id){
      doc = data[i];
      break;
    }
  }
  return doc.data;
}

export function toLongDate(timestamp, locale){
  if(locale){
    moment.locale(locale);
  }
  return moment.unix(timestamp).format("ddd D, MMM YYYY, HH:mm A");
}

export function toHumanReadable(timestamp, locale){
  if(locale){
    moment.locale(locale);
  }
  return moment.unix(timestamp).format("D MMMM YYYY");
}

export function toHourHumanReadable(timestamp, locale){
  if(locale){
    moment.locale(locale);
  }
  return moment.unix(timestamp).format("HH:mm A");
}