import React, { useEffect, useState } from 'react';
import { LogBox, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import AuthNavigation from './screens/Navigation/AuthNavigation';
import {QueryClient, QueryClientProvider} from 'react-query';
import SessionContextProvider from './context/store/Session';
import LocationContextProvider from './context/store/Location';
import DeviceContextProvider from './context/store/Device';

LogBox.ignoreAllLogs(true);

const client = new QueryClient();
const Stack = createStackNavigator();

export default function App() {


  const [loading, setLoading] = useState(false);


  useEffect(()=>{
    async function prepare() {
      
    }
    prepare();
  },[])

  const InitialNavigation = () => {
    const currentScreen = () => {
      return loading ?
      (
        <View style={{flex:1, backgroundColor:'#fff'}} />
      )
      :
      (
        <Stack.Screen
          name="AuthNavigation"
          component={AuthNavigation}
          options={{ headerShown: false }}
        />
      )
    }
    return <Stack.Navigator>{currentScreen()}</Stack.Navigator>;
  };

  return (
    <QueryClientProvider client={client}>
      <SessionContextProvider>
        <LocationContextProvider>
          <DeviceContextProvider>
            <NavigationContainer>
              <InitialNavigation />
            </NavigationContainer>
          </DeviceContextProvider>
        </LocationContextProvider>
      </SessionContextProvider>
    </QueryClientProvider>
      
  );
}

